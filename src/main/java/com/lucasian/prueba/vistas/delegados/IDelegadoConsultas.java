/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.vistas.delegados;

import java.util.List;
import java.util.Map;

/**
 * Interface de comunicaci�n para las operaciones de consulta entre los componentes gr�ficos y la l�gica
 * {@code ILogicaConsultas}.
 * 
 * @author Joan Romero
 */ 
public interface IDelegadoConsultas
{
    /**
     * {@inheritDoc}
     * @see com.trustsas.gestion.logica.consultas.ILogicaConsultas#consultarEntidades(String, Map)
     */
    public List<Object[]> consultarEntidades(String namedQuery, Map<String, Object> parametros)
            throws Exception;

    /**
     * {@inheritDoc}
     * @see com.trustsas.gestion.logica.consultas.ILogicaConsultas#consultarEntidades(String, Map, String)
     */
    public List<Object> consultarEntidades(String namedQuery, Map<String, Object> parametros, String clase)
            throws Exception;

    /**
     * {@inheritDoc}
     * @see com.trustsas.gestion.logica.consultas.ILogicaConsultas#consultarRegistro(String, Map)
     */
    public Object consultarRegistro(String namedQuery, Map<String, Object> parametros) throws Exception;
}
