/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.vistas.delegados;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.lucasian.prueba.logica.consultas.ILogicaConsultas;

/**
 * Implementa la interfaz {@code IDelegadoConsultas} y todas las operaciones de consulta soportadas.
 * 
 * @author Joan Romero
 */
@Component("delegadoConsultas")
@Scope("singleton")
public class DelegadoConsultas implements IDelegadoConsultas
{
    @Autowired
    private ILogicaConsultas logicaConsultas;
    
    @Override
    public List<Object[]> consultarEntidades(String namedQuery, Map<String, Object> parametros)
            throws Exception
    {
        return logicaConsultas.consultarEntidades(namedQuery, parametros);
    }

    @Override
    public List<Object> consultarEntidades(String namedQuery, Map<String, Object> parametros, String clase)
            throws Exception
    {
        return logicaConsultas.consultarEntidades(namedQuery, parametros, clase);
    }

    @Override
    public Object consultarRegistro(String namedQuery, Map<String, Object> parametros) throws Exception
    {
        return logicaConsultas.consultarRegistro(namedQuery, parametros);
    }
}
