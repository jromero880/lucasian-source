/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.vistas.delegados;

import java.util.List;

import com.lucasian.prueba.dto.procesos.FacturaDetalleDTO;
import com.lucasian.prueba.entidades.tablas.Articulo;
import com.lucasian.prueba.entidades.tablas.Cliente;
import com.lucasian.prueba.entidades.tablas.Factura;
import com.lucasian.prueba.entidades.tablas.Proveedor;

/**
 * Interface de comunicaci�n para las operaciones entre los componentes gr�ficos y las l�gicas de Procesos.
 * Se tienen las siguientes l�gicas:
 * <ul>
 * <li>{@code ILogicaProveedor}
 * <li>{@code ILogicaCliente}
 * <li>{@code ILogicaArticulo}
 * <li>{@code ILogicaFactura}
 * </ul>
 * Adicional, extiende de {@code IDelegadoConsultas} para soportar todas las operaciones de consultas.
 * 
 * @author Joan Romero
 */
public interface IDelegadoProcesos extends IDelegadoConsultas
{
    /**
     * {@inheritDoc}
     * @see com.lucasian.prueba.logica.procesos.ILogicaProveedor#guardarProveedor(Proveedor)
     */
    public String guardarProveedor(Proveedor proveedor) throws Exception;
    
    /**
     * {@inheritDoc}
     * @see com.lucasian.prueba.logica.procesos.ILogicaProveedor#editarProveedor(Proveedor)
     */
    public String editarProveedor(Proveedor proveedor) throws Exception;
    
    /**
     * {@inheritDoc}
     * @see com.lucasian.prueba.logica.procesos.ILogicaProveedor#eliminarProveedor(Proveedor)
     */
    public String eliminarProveedor(Proveedor proveedor) throws Exception;
    
    /**
     * {@inheritDoc}
     * @see com.lucasian.prueba.logica.procesos.ILogicaCliente#guardarCliente(Cliente)
     */
    public String guardarCliente(Cliente cliente) throws Exception;
    
    /**
     * {@inheritDoc}
     * @see com.lucasian.prueba.logica.procesos.ILogicaCliente#editarCliente(Cliente)
     */
    public String editarCliente(Cliente cliente) throws Exception;
    
    /**
     * {@inheritDoc}
     * @see com.lucasian.prueba.logica.procesos.ILogicaCliente#eliminarCliente(Cliente)
     */
    public String eliminarCliente(Cliente cliente) throws Exception;
    
    /**
     * {@inheritDoc}
     * @see com.lucasian.prueba.logica.procesos.ILogicaArticulo#guardarArticulo(Articulo)
     */
    public String guardarArticulo(Articulo articulo) throws Exception;
    
    /**
     * {@inheritDoc}
     * @see com.lucasian.prueba.logica.procesos.ILogicaArticulo#editarArticulo(Articulo)
     */
    public String editarArticulo(Articulo articulo) throws Exception;
    
    /**
     * {@inheritDoc}
     * @see com.lucasian.prueba.logica.procesos.ILogicaArticulo#eliminarArticulo(Articulo)
     */
    public String eliminarArticulo(Articulo articulo) throws Exception;
    
    /**
     * {@inheritDoc}
     * @see com.lucasian.prueba.logica.procesos.ILogicaFactura#getNumeroFactura()
     */
    public int getNumeroFactura() throws Exception;
    
    /**
     * {@inheritDoc}
     * @see com.lucasian.prueba.logica.procesos.ILogicaFactura#getDetalleFactura(List)
     */
    public int getDetalleFactura(List<FacturaDetalleDTO> articulos);
    
    /**
     * {@inheritDoc}
     * @see com.lucasian.prueba.logica.procesos.ILogicaFactura#guardarFactura(Factura, List)
     */
    public String guardarFactura(Factura factura, List<FacturaDetalleDTO> articulos) throws Exception;
}
