/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.vistas.delegados;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.lucasian.prueba.dto.procesos.FacturaDetalleDTO;
import com.lucasian.prueba.entidades.tablas.Articulo;
import com.lucasian.prueba.entidades.tablas.Cliente;
import com.lucasian.prueba.entidades.tablas.Factura;
import com.lucasian.prueba.entidades.tablas.Proveedor;
import com.lucasian.prueba.logica.procesos.ILogicaArticulo;
import com.lucasian.prueba.logica.procesos.ILogicaCliente;
import com.lucasian.prueba.logica.procesos.ILogicaFactura;
import com.lucasian.prueba.logica.procesos.ILogicaProveedor;

/**
 * Implementa la interfaz {@code IDelegadoProcesos} y todas las operaciones soportadas. Extiende de 
 * {@code DelegadoConsultas} para soportar todas las operaciones de consultas.
 * 
 * @author Joan Romero
 */
@Scope("singleton")
@Component("delegadoProcesos")
public class DelegadoProcesos extends DelegadoConsultas implements IDelegadoProcesos
{
    @Autowired
    private ILogicaProveedor logicaProveedor;
    
    @Autowired
    private ILogicaCliente logicaCliente;
    
    @Autowired
    private ILogicaArticulo logicaArticulo;
    
    @Autowired
    private ILogicaFactura logicaFactura;
    
    @Override
    public String guardarProveedor(Proveedor proveedor) throws Exception
    {
        return logicaProveedor.guardarProveedor(proveedor);
    }

    @Override
    public String editarProveedor(Proveedor proveedor) throws Exception
    {
        return logicaProveedor.editarProveedor(proveedor);
    }

    @Override
    public String eliminarProveedor(Proveedor proveedor) throws Exception
    {
        return logicaProveedor.eliminarProveedor(proveedor);
    }

    @Override
    public String guardarCliente(Cliente cliente) throws Exception
    {
        return logicaCliente.guardarCliente(cliente);
    }

    @Override
    public String editarCliente(Cliente cliente) throws Exception
    {
        return logicaCliente.editarCliente(cliente);
    }

    @Override
    public String eliminarCliente(Cliente cliente) throws Exception
    {
        return logicaCliente.eliminarCliente(cliente);
    }

    @Override
    public String guardarArticulo(Articulo articulo) throws Exception
    {
        return logicaArticulo.guardarArticulo(articulo);
    }

    @Override
    public String editarArticulo(Articulo articulo) throws Exception
    {
        return logicaArticulo.editarArticulo(articulo);
    }

    @Override
    public String eliminarArticulo(Articulo articulo) throws Exception
    {
        return logicaArticulo.eliminarArticulo(articulo);
    }

    @Override
    public int getNumeroFactura() throws Exception
    {
        return logicaFactura.getNumeroFactura();
    }

    @Override
    public int getDetalleFactura(List<FacturaDetalleDTO> articulos)
    {
        return logicaFactura.getDetalleFactura(articulos);
    }

    @Override
    public String guardarFactura(Factura factura, List<FacturaDetalleDTO> articulos) throws Exception
    {
        return logicaFactura.guardarFactura(factura, articulos);
    }
}
