/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.vistas;

/**
 * Listado de posibles estados que puede llegar a tener una interfaz en la aplicación.
 * 
 * @author Joan Romero
 */
public enum Estados
{
    CREANDO,
    MODIFICANDO,
    CONSULTANDO
}
