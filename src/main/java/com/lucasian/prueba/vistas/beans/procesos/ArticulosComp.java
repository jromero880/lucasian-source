/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.vistas.beans.procesos;

import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.inputtextarea.InputTextarea;

import com.lucasian.prueba.utilidades.comunes.Datos;
import com.lucasian.prueba.vistas.Campos;

/**
 * Define los componentes y m�todos generales a ser utilizados en el formulario de Art�culos.
 * 
 * @author Joan Romero
 */
public class ArticulosComp
{
    protected InputText codigo;
    protected InputText nombre;
    protected InputText codProv;
    protected InputText nomProv;
    protected InputTextarea caracteristicas;
    
    protected CommandButton modificar;
    protected CommandButton limpiar;
    protected CommandButton cancelar;
    protected CommandButton eliminar;
    protected CommandButton guardar;
    
    /**
     * Genera una instancia para {@code ArticulosComp}.
     */
    public ArticulosComp()
    {
        codigo = new InputText();
        nombre = new InputText();
        codProv = new InputText();
        nomProv = new InputText();
        caracteristicas = new InputTextarea();
        
        inicializarBotones();
    }

    public InputText getCodigo()
    {
        return codigo;
    }

    public void setCodigo(InputText codigo)
    {
        this.codigo = codigo;
    }

    public InputText getNombre()
    {
        return nombre;
    }

    public void setNombre(InputText nombre)
    {
        this.nombre = nombre;
    }

    public InputText getCodProv()
    {
        return codProv;
    }

    public void setCodProv(InputText codProv)
    {
        this.codProv = codProv;
    }

    public InputText getNomProv()
    {
        return nomProv;
    }

    public void setNomProv(InputText nomProv)
    {
        this.nomProv = nomProv;
    }

    public InputTextarea getCaracteristicas()
    {
        return caracteristicas;
    }

    public void setCaracteristicas(InputTextarea caracteristicas)
    {
        this.caracteristicas = caracteristicas;
    }

    public CommandButton getModificar()
    {
        return modificar;
    }

    public void setModificar(CommandButton modificar)
    {
        this.modificar = modificar;
    }

    public CommandButton getLimpiar()
    {
        return limpiar;
    }

    public void setLimpiar(CommandButton limpiar)
    {
        this.limpiar = limpiar;
    }

    public CommandButton getCancelar()
    {
        return cancelar;
    }

    public void setCancelar(CommandButton cancelar)
    {
        this.cancelar = cancelar;
    }

    public CommandButton getEliminar()
    {
        return eliminar;
    }

    public void setEliminar(CommandButton eliminar)
    {
        this.eliminar = eliminar;
    }

    public CommandButton getGuardar()
    {
        return guardar;
    }

    public void setGuardar(CommandButton guardar)
    {
        this.guardar = guardar;
    }

    /**
     * Instancia los botones y establece su estado por defecto.
     */
    protected void inicializarBotones()
    {
        modificar = new CommandButton();
        limpiar = new CommandButton();
        cancelar = new CommandButton();
        eliminar = new CommandButton();
        guardar = new CommandButton();
        
        boolean[] eb = { false, true, false, false, true };
        setEstadoBotones(eb);
    }
    
    /**
     * Establece los valores para los componentes del formulario.
     *
     * @param d Valores para los componentes.
     */
    protected void setValuesCampos(Object[] d)
    {
        d = Datos.cambiarNull(d);
        
        codigo.setValue(d[0]);
        nombre.setValue(d[1]);
        codProv.setValue(d[2]);
        nomProv.setValue(d[3]);
        caracteristicas.setValue(d[4]);
    }
    
    /**
     * Establece si se habilitan o no los componentes dentro del formulario.
     *
     * @param b Estados para los componentes.
     */
    protected void setEnabledCampos(boolean[] b)
    {
        codigo.setReadonly(!b[0]);
        nombre.setReadonly(!b[1]);
        codProv.setReadonly(!b[2]);
        caracteristicas.setReadonly(!b[3]);
    }
    
    /**
     * Establece si se habilitan o no los botones dentro del formulario.
     *
     * @param b Estados para los botones.
     */
    protected void setEstadoBotones(boolean[] b)
    {
        modificar.setDisabled(!b[0]);
        limpiar.setDisabled(!b[1]);
        cancelar.setDisabled(!b[2]);
        eliminar.setDisabled(!b[3]);
        guardar.setDisabled(!b[4]);
    }
    
    /**
     * Obtiene los componentes que son obligatorios en el formulario.
     * 
     * @return Lista de componentes obligatorios.
     */
    protected Object[] getComponentes()
    {
        return new Object[] { codigo, nombre, codProv, nomProv, caracteristicas };
    }
    
    /**
     * Obtiene los tipos de componentes que son obligatorios en el formulario.
     * 
     * @return Lista de tipos de componentes.
     */
    protected Campos[] getTipos()
    {
        return new Campos[] { Campos.INPUTTEXT, Campos.INPUTTEXT, Campos.INPUTTEXT, Campos.INPUTTEXT,
            Campos.INPUTTEXTAREA };
    }
}
