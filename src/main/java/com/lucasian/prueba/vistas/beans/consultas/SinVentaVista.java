/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.vistas.beans.consultas;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.lucasian.prueba.dto.consultas.ArticulosFacturaDTO;
import com.lucasian.prueba.entidades.QuerysMod;
import com.lucasian.prueba.utilidades.comunes.Mensajes;
import com.lucasian.prueba.vistas.delegados.IDelegadoConsultas;

/**
 * Maneja los eventos y procesos realizados en el formulario de Sin Venta.
 * 
 * @author Joan Romero
 */
@ViewScoped
@ManagedBean
public class SinVentaVista extends TopVentaComp
{
    /**
     * Delegado para realizar los procesos del formulario.
     */
    @ManagedProperty(value="#{delegadoConsultas}")
    private IDelegadoConsultas delegadoConsultas;
    
    /**
     * Genera una instancia para {@code SinVentaVista}.
     */
    public SinVentaVista()
    {
        super();
    }

    /**
     * Obtiene el delegado para realizar los procesos del formulario.
     * 
     * @return Delegado para realizar los procesos del formulario.
     */
    public IDelegadoConsultas getDelegadoConsultas()
    {
        return delegadoConsultas;
    }

    /**
     * Fija el delegado para realizar los procesos del formulario.
     * 
     * @param delegadoConsultas Delegado para realizar los procesos del formulario.
     */
    public void setDelegadoConsultas(IDelegadoConsultas delegadoConsultas)
    {
        this.delegadoConsultas = delegadoConsultas;
    }
    
    /**
     * Evento al presionar el bot�n de Consultar.
     */
    public void accionConsultar()
    {
        consultarDatos();
    }
    
    /**
     * Consulta los art�culos sin venta por mes.
     */
    public void consultarDatos()
    {
        try
        {
            Map<String, Object> param = new HashMap<>();
            
            List<Object> data = delegadoConsultas.consultarEntidades(QuerysMod.CONS_ART_SIN_VENTAS_MES, param,
                "com.lucasian.prueba.dto.consultas.ArticulosFacturaDTO");
            
            articulos.clear();
            
            if (!data.isEmpty())
            {
                for (Object d : data)
                {
                    ArticulosFacturaDTO af = (ArticulosFacturaDTO)d;
                    articulos.add(af);
                }
            }
            else
            {
                Mensajes.mostrarAdv("Advertencia", "No hay registros para mostrar");
            }
        }
        catch (Exception ex)
        {
            Mensajes.errorConsulta(ex);
        }
    }
}
