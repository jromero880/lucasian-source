/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.vistas.beans.principal;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.lucasian.prueba.utilidades.comunes.Faces;

/**
 * Maneja procesos y constantes generales para toda la aplicación.
 * 
 * @author Joan Romero
 */
@ViewScoped
@ManagedBean
public class GeneralVista
{
    /**
     * URL general de la aplicación.
     */
    private String url;
    
    /**
     * Ruta del contexto de la aplicación.
     */
    private String conPath;
    
    /**
     * Genera una instancia para {@code GeneralVista}.
     */
    public GeneralVista()
    {
        url = Faces.getURL();
        conPath = Faces.getContextPath();
    }
    
    public String getUrl()
    {
        return url;
    }

    public String getConPath()
    {
        return conPath;
    }
}
