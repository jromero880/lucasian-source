/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.vistas.beans.procesos;

import java.util.ArrayList;
import java.util.List;

import org.primefaces.component.calendar.Calendar;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputnumber.InputNumber;
import org.primefaces.component.inputtext.InputText;

import com.lucasian.prueba.dto.procesos.FacturaDetalleDTO;
import com.lucasian.prueba.utilidades.comunes.Datos;
import com.lucasian.prueba.vistas.Campos;

/**
 * Define los componentes y m�todos generales a ser utilizados en el formulario de Facturas.
 * 
 * @author Joan Romero
 */
public class FacturasComp
{
    protected InputText numero;
    protected Calendar fecha;
    protected InputText codClie;
    protected InputText nomClie;
    
    protected List<FacturaDetalleDTO> articulos;
    protected FacturaDetalleDTO selArticulo;
    
    protected CommandButton anadirArt;
    protected CommandButton removerArt;
    
    protected InputText detalle;
    protected InputText codArt;
    protected InputText nomArt;
    protected InputNumber cantidad;
    protected InputNumber precio;
    
    protected CommandButton limpiar;
    protected CommandButton guardar;
    
    /**
     * Genera una instancia para {@code FacturasComp}.
     */
    public FacturasComp()
    {
        numero = new InputText();
        fecha = new Calendar();
        codClie = new InputText();
        nomClie = new InputText();
        
        articulos = new ArrayList<>();
        
        anadirArt = new CommandButton();
        removerArt = new CommandButton();
        
        detalle = new InputText();
        codArt = new InputText();
        nomArt = new InputText();
        cantidad = new InputNumber();
        precio = new InputNumber();
        
        inicializarBotones();
    }

    public InputText getNumero()
    {
        return numero;
    }

    public void setNumero(InputText numero)
    {
        this.numero = numero;
    }

    public Calendar getFecha()
    {
        return fecha;
    }

    public void setFecha(Calendar fecha)
    {
        this.fecha = fecha;
    }

    public InputText getCodClie()
    {
        return codClie;
    }

    public void setCodClie(InputText codClie)
    {
        this.codClie = codClie;
    }

    public InputText getNomClie()
    {
        return nomClie;
    }

    public void setNomClie(InputText nomClie)
    {
        this.nomClie = nomClie;
    }

    public List<FacturaDetalleDTO> getArticulos()
    {
        return articulos;
    }

    public void setArticulos(List<FacturaDetalleDTO> articulos)
    {
        this.articulos = articulos;
    }

    public FacturaDetalleDTO getSelArticulo()
    {
        return selArticulo;
    }

    public void setSelArticulo(FacturaDetalleDTO selArticulo)
    {
        this.selArticulo = selArticulo;
    }

    public CommandButton getAnadirArt()
    {
        return anadirArt;
    }

    public void setAnadirArt(CommandButton anadirArt)
    {
        this.anadirArt = anadirArt;
    }

    public CommandButton getRemoverArt()
    {
        return removerArt;
    }

    public void setRemoverArt(CommandButton removerArt)
    {
        this.removerArt = removerArt;
    }

    public InputText getDetalle()
    {
        return detalle;
    }

    public void setDetalle(InputText detalle)
    {
        this.detalle = detalle;
    }

    public InputText getCodArt()
    {
        return codArt;
    }

    public void setCodArt(InputText codArt)
    {
        this.codArt = codArt;
    }

    public InputText getNomArt()
    {
        return nomArt;
    }

    public void setNomArt(InputText nomArt)
    {
        this.nomArt = nomArt;
    }

    public InputNumber getCantidad()
    {
        return cantidad;
    }

    public void setCantidad(InputNumber cantidad)
    {
        this.cantidad = cantidad;
    }

    public InputNumber getPrecio()
    {
        return precio;
    }

    public void setPrecio(InputNumber precio)
    {
        this.precio = precio;
    }

    public CommandButton getLimpiar()
    {
        return limpiar;
    }

    public void setLimpiar(CommandButton limpiar)
    {
        this.limpiar = limpiar;
    }

    public CommandButton getGuardar()
    {
        return guardar;
    }

    public void setGuardar(CommandButton guardar)
    {
        this.guardar = guardar;
    }

    /**
     * Instancia los botones y establece su estado por defecto.
     */
    protected void inicializarBotones()
    {
        limpiar = new CommandButton();
        guardar = new CommandButton();
        
        boolean[] eb = { true, true };
        setEstadoBotones(eb);
    }
    
    /**
     * Establece los valores para los componentes del formulario.
     *
     * @param d Valores para los componentes.
     */
    protected void setValuesCampos(Object[] d)
    {
        d = Datos.cambiarNull(d);
        
        numero.setValue(d[0]);
        fecha.setValue(d[1]);
        codClie.setValue(d[2]);
        nomClie.setValue(d[3]);
    }
    
    /**
     * Establece los valores para los componentes de art�culos en el formulario.
     *
     * @param d Valores para los componentes.
     */
    protected void setValuesArticulos(Object[] d)
    {
        d = Datos.cambiarNull(d);
        
        detalle.setValue(d[0]);
        codArt.setValue(d[1]);
        nomArt.setValue(d[2]);
        cantidad.setValue(d[3]);
        precio.setValue(d[4]);
    }
    
    /**
     * Establece si se habilitan o no los componentes dentro del formulario.
     *
     * @param b Estados para los componentes.
     */
    protected void setEnabledCampos(boolean[] b)
    {
        numero.setReadonly(!b[0]);
        fecha.setDisabled(!b[1]);
        codClie.setReadonly(!b[2]);
    }
    
    /**
     * Establece si se habilitan o no los componentes de art�culos.
     *
     * @param b Estados para los componentes.
     */
    protected void setEnabledArticulos(boolean[] b)
    {
        anadirArt.setDisabled(!b[0]);
        removerArt.setDisabled(!b[1]);
        codArt.setReadonly(!b[2]);
        cantidad.setReadonly(!b[3]);
        precio.setReadonly(!b[4]);
    }
    
    /**
     * Establece si se habilitan o no los botones dentro del formulario.
     *
     * @param b Estados para los botones.
     */
    protected void setEstadoBotones(boolean[] b)
    {
        limpiar.setDisabled(!b[0]);
        guardar.setDisabled(!b[1]);
    }
    
    /**
     * Obtiene los componentes que son obligatorios en el formulario.
     * 
     * @return Lista de componentes obligatorios.
     */
    protected Object[] getComponentes()
    {
        return new Object[] { numero, fecha, codClie, nomClie };
    }
    
    /**
     * Obtiene los tipos de componentes que son obligatorios en el formulario.
     * 
     * @return Lista de tipos de componentes.
     */
    protected Campos[] getTipos()
    {
        return new Campos[] { Campos.INPUTTEXT, Campos.CALENDAR, Campos.INPUTTEXT, Campos.INPUTTEXT };
    }
    
    /**
     * Obtiene los componentes que son obligatorios en art�culos.
     * 
     * @return Lista de componentes obligatorios.
     */
    protected Object[] getComponentesArticulo()
    {
        return new Object[] { detalle, codArt, nomArt, cantidad, precio };
    }
    
    /**
     * Obtiene los tipos de componentes que son obligatorios en art�culos.
     * 
     * @return Lista de tipos de componentes.
     */
    protected Campos[] getTiposArticulo()
    {
        return new Campos[] { Campos.INPUTTEXT, Campos.INPUTTEXT, Campos.INPUTTEXT, Campos.INPUTNUMBER,
            Campos.INPUTNUMBER };
    }
}
