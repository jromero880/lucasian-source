/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.vistas.beans.consultas;

import java.util.ArrayList;
import java.util.List;

import org.primefaces.component.commandbutton.CommandButton;

import com.lucasian.prueba.dto.consultas.ArticulosFacturaDTO;

/**
 * Define los componentes y m�todos generales a ser utilizados en el formulario de Top Venta.
 * 
 * @author Joan Romero
 */
public class TopVentaComp
{
    protected List<ArticulosFacturaDTO> articulos;
    
    protected CommandButton consultar;
    
    /**
     * Genera una instancia para {@code TopVentaComp}.
     */
    public TopVentaComp()
    {
        articulos = new ArrayList<>();
        
        inicializarBotones();
    }

    public List<ArticulosFacturaDTO> getArticulos()
    {
        return articulos;
    }

    public void setArticulos(List<ArticulosFacturaDTO> articulos)
    {
        this.articulos = articulos;
    }

    public CommandButton getConsultar()
    {
        return consultar;
    }

    public void setConsultar(CommandButton consultar)
    {
        this.consultar = consultar;
    }

    /**
     * Instancia los botones y establece su estado por defecto.
     */
    protected void inicializarBotones()
    {
        consultar = new CommandButton();
        
        boolean[] eb = { true };
        setEstadoBotones(eb);
    }
    
    /**
     * Establece si se habilitan o no los botones dentro del formulario.
     *
     * @param b Estados para los botones.
     */
    protected void setEstadoBotones(boolean[] b)
    {
        consultar.setDisabled(!b[0]);
    }
}
