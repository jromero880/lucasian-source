/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.vistas.beans.procesos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.lucasian.prueba.dto.procesos.ProveedorDTO;
import com.lucasian.prueba.entidades.QuerysMod;
import com.lucasian.prueba.entidades.tablas.Proveedor;
import com.lucasian.prueba.utilidades.comunes.Componentes;
import com.lucasian.prueba.utilidades.comunes.Mensajes;
import com.lucasian.prueba.vistas.Estados;
import com.lucasian.prueba.vistas.delegados.IDelegadoProcesos;

/**
 * Maneja los eventos y procesos realizados en el formulario de Proveedores.
 * 
 * @author Joan Romero
 */
@ViewScoped
@ManagedBean
public class ProveedoresVista extends ProveedoresComp
{
    /**
     * Descripci�n para mostrar en los mensajes de formulario.
     */
    private static final String MSJ_MODULO = "el proveedor";
    
    /**
     * Estado actual en el formulario.
     */
    private Estados estado = Estados.CREANDO;
    
    /**
     * Delegado para realizar los procesos del formulario.
     */
    @ManagedProperty(value="#{delegadoProcesos}")
    private IDelegadoProcesos delegadoProcesos;
    
    /**
     * Genera una instancia para {@code ProveedoresVista}.
     */
    public ProveedoresVista()
    {
        super();
    }

    /**
     * Obtiene el delegado para realizar los procesos del formulario.
     * 
     * @return Delegado para realizar los procesos del formulario.
     */
    public IDelegadoProcesos getDelegadoProcesos()
    {
        return delegadoProcesos;
    }

    /**
     * Fija el delegado para realizar los procesos del formulario.
     * 
     * @param delegadoProcesos Delegado para realizar los procesos del formulario.
     */
    public void setDelegadoProcesos(IDelegadoProcesos delegadoProcesos)
    {
        this.delegadoProcesos = delegadoProcesos;
    }
    
    /**
     * Evento al presionar el bot�n de Modificar.
     */
    public void accionModificar()
    {
        habilitarModificar();
    }
    
    /**
     * Evento al presionar el bot�n de Limpiar.
     */
    public void accionLimpiar()
    {
        limpiarCampos();
    }
    
    /**
     * Evento al presionar el bot�n de Cancelar.
     */
    public void accionCancelar()
    {
        estado = Estados.CONSULTANDO;
        consultarDatos();
    }
    
    /**
     * Evento al presionar el bot�n de Eliminar.
     */
    public void accionEliminar()
    {
        eliminarRegistro();
    }
    
    /**
     * Evento al presionar el bot�n de Guardar.
     */
    public void accionGuardar()
    {
        guardarRegistro();
    }
    
    /**
     * Evento al cambiar el valor del campo de C�digo.
     */
    public void codigoOnChange()
    {
        consultarDatos();
    }
    
    /**
     * Consulta un proveedor.
     */
    public void consultarDatos()
    {
        try
        {
            if (estado != Estados.MODIFICANDO)
            {
                String cod = codigo.getValue().toString();
                if (!cod.equals(""))
                {
                    Map<String, Object> param = new HashMap<>();
                    param.put("proveedor", cod);
                    
                    List<Object> data = delegadoProcesos.consultarEntidades(QuerysMod.PROC_PROVEEDORES,
                        param, "com.lucasian.prueba.dto.procesos.ProveedorDTO");
                    
                    if (!data.isEmpty())
                    {
                        ProveedorDTO p = (ProveedorDTO)data.get(0);
                        
                        Object[] d = { p.getCif(), p.getNombre(), p.getApellido1(), p.getApellido2(),
                            p.getEmpresa(), p.getDireccion(), p.getCiudad(), p.getCp(), p.getProvincia(),
                            p.getPais() };
                        
                        boolean[] b = { false, false, false, false, false, false, false, false, false,
                            false };
                        
                        setValuesCampos(d);
                        setEnabledCampos(b);
                        
                        boolean[] eb = { true, true, false, true, false };
                        setEstadoBotones(eb);
                        
                        estado = Estados.CONSULTANDO;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Mensajes.errorConsulta(ex);
        }
    }
    
    /**
     * Habilita los campos y los botones para realizar la modificaci�n del registro.
     */
    private void habilitarModificar()
    {
        boolean[] b = { false, true, true, true, true, true, true, true, true, true };
        setEnabledCampos(b);
        
        boolean[] eb = { false, false, true, false, true };
        setEstadoBotones(eb);
        estado = Estados.MODIFICANDO;  
    }
    
    /**
     * Limpia los campos del formulario y establece el estado de los botones.
     */
    private void limpiarCampos()
    {
        Object[] d = { "", "", "", "", "", "", "", "", "", "" };
        boolean[] b = { true, true, true, true, true, true, true, true, true, true };
        
        setValuesCampos(d);
        setEnabledCampos(b);

        boolean[] eb = { false, true, false, false, true };
        setEstadoBotones(eb);
        estado = Estados.CREANDO;
    }
    
    /**
     * Elimina un proveedor de la base de datos.
     */
    private void eliminarRegistro()
    {
        try
        {
            delegadoProcesos.eliminarProveedor(getProveedor());
            Mensajes.infoEliminar(MSJ_MODULO);
            limpiarCampos();
        }
        catch (Exception ex)
        {
            Mensajes.errorEliminar(MSJ_MODULO, ex);
        }
    }
    
    /**
     * Guarda un proveedor en la base de datos.
     */
    private void guardarRegistro()
    {
        boolean validos = Componentes.validarCampos(getComponentes(), getTipos());
        if (!validos)
        {
            Mensajes.camposVacios();
        }
        else
        {
            try
            { 
                Proveedor proveedor = getProveedor();
                
                if (estado == Estados.CREANDO)
                {
                    delegadoProcesos.guardarProveedor(proveedor);
                }
                else if (estado == Estados.MODIFICANDO)
                {
                    delegadoProcesos.editarProveedor(proveedor);
                }
                
                estado = Estados.CONSULTANDO;
                consultarDatos();
                Mensajes.infoActualizar(MSJ_MODULO);
            }
            catch (Exception ex)
            {
                Mensajes.errorActualizar(MSJ_MODULO, ex);
            }
        }
    }
    
    /**
     * Obtiene la entidad de proveedor para almacenar en la base de datos.
     * 
     * @return Proveedor para almacenar.
     */
    private Proveedor getProveedor()
    {
        Proveedor proveedor = new Proveedor();
        
        proveedor.setCif(codigo.getValue().toString().toUpperCase());
        proveedor.setNombre(nombre.getValue().toString().toUpperCase());
        proveedor.setApellido1(apellido1.getValue().toString().toUpperCase());
        
        if (apellido2.getValue() != null && !apellido2.getValue().toString().equals(""))
        {
            proveedor.setApellido2(apellido2.getValue().toString().toUpperCase());
        }
        
        proveedor.setEmpresa(empresa.getValue().toString().toUpperCase());
        proveedor.setDireccion(direccion.getValue().toString().toUpperCase());
        proveedor.setCiudad(ciudad.getValue().toString().toUpperCase());
        proveedor.setCp(cp.getValue().toString().toUpperCase());
        proveedor.setProvincia(provincia.getValue().toString().toUpperCase());
        proveedor.setPais(pais.getValue().toString().toUpperCase());
        
        return proveedor;
    }
}
