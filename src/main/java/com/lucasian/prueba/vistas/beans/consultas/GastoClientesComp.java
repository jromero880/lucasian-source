/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.vistas.beans.consultas;

import java.util.ArrayList;
import java.util.List;

import org.primefaces.component.commandbutton.CommandButton;

import com.lucasian.prueba.dto.consultas.InformeFacturasDTO;

/**
 * Define los componentes y m�todos generales a ser utilizados en el formulario de Gasto Clientes.
 * 
 * @author Joan Romero
 */
public class GastoClientesComp
{
    protected List<InformeFacturasDTO> clientes;
    
    protected CommandButton consultar;
    
    /**
     * Genera una instancia para {@code GastoClientesComp}.
     */
    public GastoClientesComp()
    {
        clientes = new ArrayList<>();
        
        inicializarBotones();
    }

    public List<InformeFacturasDTO> getClientes()
    {
        return clientes;
    }

    public void setClientes(List<InformeFacturasDTO> clientes)
    {
        this.clientes = clientes;
    }

    public CommandButton getConsultar()
    {
        return consultar;
    }

    public void setConsultar(CommandButton consultar)
    {
        this.consultar = consultar;
    }

    /**
     * Instancia los botones y establece su estado por defecto.
     */
    protected void inicializarBotones()
    {
        consultar = new CommandButton();
        
        boolean[] eb = { true };
        setEstadoBotones(eb);
    }
    
    /**
     * Establece si se habilitan o no los botones dentro del formulario.
     *
     * @param b Estados para los botones.
     */
    protected void setEstadoBotones(boolean[] b)
    {
        consultar.setDisabled(!b[0]);
    }
}
