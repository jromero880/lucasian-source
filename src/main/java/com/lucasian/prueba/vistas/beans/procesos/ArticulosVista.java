/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.vistas.beans.procesos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.lucasian.prueba.dto.procesos.ArticuloDTO;
import com.lucasian.prueba.entidades.QuerysComp;
import com.lucasian.prueba.entidades.QuerysMod;
import com.lucasian.prueba.entidades.tablas.Articulo;
import com.lucasian.prueba.utilidades.comunes.Componentes;
import com.lucasian.prueba.utilidades.comunes.Mensajes;
import com.lucasian.prueba.vistas.Campos;
import com.lucasian.prueba.vistas.Estados;
import com.lucasian.prueba.vistas.delegados.IDelegadoProcesos;

/**
 * Maneja los eventos y procesos realizados en el formulario de Art�culos.
 * 
 * @author Joan Romero
 */
@ViewScoped
@ManagedBean
public class ArticulosVista extends ArticulosComp
{
    /**
     * Descripci�n para mostrar en los mensajes de formulario.
     */
    private static final String MSJ_MODULO = "el art�culo";
    
    /**
     * Estado actual en el formulario.
     */
    private Estados estado = Estados.CREANDO;
    
    /**
     * Delegado para realizar los procesos del formulario.
     */
    @ManagedProperty(value="#{delegadoProcesos}")
    private IDelegadoProcesos delegadoProcesos;
    
    /**
     * Genera una instancia para {@code ArticulosVista}.
     */
    public ArticulosVista()
    {
        super();
    }

    /**
     * Obtiene el delegado para realizar los procesos del formulario.
     * 
     * @return Delegado para realizar los procesos del formulario.
     */
    public IDelegadoProcesos getDelegadoProcesos()
    {
        return delegadoProcesos;
    }

    /**
     * Fija el delegado para realizar los procesos del formulario.
     * 
     * @param delegadoProcesos Delegado para realizar los procesos del formulario.
     */
    public void setDelegadoProcesos(IDelegadoProcesos delegadoProcesos)
    {
        this.delegadoProcesos = delegadoProcesos;
    }
    
    /**
     * Evento al presionar el bot�n de Modificar.
     */
    public void accionModificar()
    {
        habilitarModificar();
    }
    
    /**
     * Evento al presionar el bot�n de Limpiar.
     */
    public void accionLimpiar()
    {
        limpiarCampos();
    }
    
    /**
     * Evento al presionar el bot�n de Cancelar.
     */
    public void accionCancelar()
    {
        estado = Estados.CONSULTANDO;
        consultarDatos();
    }
    
    /**
     * Evento al presionar el bot�n de Eliminar.
     */
    public void accionEliminar()
    {
        eliminarRegistro();
    }
    
    /**
     * Evento al presionar el bot�n de Guardar.
     */
    public void accionGuardar()
    {
        guardarRegistro();
    }
    
    /**
     * Evento al cambiar el valor del campo de C�digo.
     */
    public void codigoOnChange()
    {
        consultarDatos();
    }
    
    /**
     * Evento al cambiar el valor del campo de C�digo Proveedor.
     */
    public void codProvOnChange()
    {
        consultarProveedor();
    }
    
    /**
     * Consulta un art�culo.
     */
    public void consultarDatos()
    {
        try
        {
            if (estado != Estados.MODIFICANDO)
            {
                String cod = codigo.getValue().toString();
                if (!cod.equals(""))
                {
                    Map<String, Object> param = new HashMap<>();
                    param.put("articulo", cod);
                    
                    List<Object> data = delegadoProcesos.consultarEntidades(QuerysMod.PROC_ARTICULOS, param,
                        "com.lucasian.prueba.dto.procesos.ArticuloDTO");
                    
                    if (!data.isEmpty())
                    {
                        ArticuloDTO a = (ArticuloDTO)data.get(0);
                        
                        Object[] d = { a.getCodigo(), a.getNombre(), a.getCodigoProv(), a.getNombreProv(),
                            a.getCaracteristicas() };
                        
                        boolean[] b = { false, false, false, false };
                        
                        setValuesCampos(d);
                        setEnabledCampos(b);
                        
                        boolean[] eb = { true, true, false, true, false };
                        setEstadoBotones(eb);
                        
                        estado = Estados.CONSULTANDO;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Mensajes.errorConsulta(ex);
        }
    }
    
    /**
     * Consulta un proveedor.
     */
    private void consultarProveedor()
    {
        try
        {
            String prov = codProv.getValue().toString();
            
            Map<String, Object> param = new HashMap<>();
            param.put("proveedor", prov);
            
            List<Object[]> data = delegadoProcesos.consultarEntidades(QuerysComp.PROC_PROVEEDOR, param);
            
            Object[] comp = { codProv, nomProv };
            Campos[] tipoComp = { Campos.INPUTTEXT, Campos.INPUTTEXT };
            
            Componentes.setDatosConsultaExternos(estado, data, comp, tipoComp, 0);
            
            if (data.isEmpty())
            {
                Mensajes.mostrarAdv("Advertencia", "El proveedor no se encuentra registrado");
            }
        }
        catch (Exception ex)
        {
            Mensajes.errorConsulta(ex);
        }
    }
    
    /**
     * Habilita los campos y los botones para realizar la modificaci�n del registro.
     */
    private void habilitarModificar()
    {
        boolean[] b = { false, true, true, true };
        setEnabledCampos(b);
        
        boolean[] eb = { false, false, true, false, true };
        setEstadoBotones(eb);
        estado = Estados.MODIFICANDO;  
    }
    
    /**
     * Limpia los campos del formulario y establece el estado de los botones.
     */
    private void limpiarCampos()
    {
        Object[] d = { "", "", "", "", "" };
        boolean[] b = { true, true, true, true };
        
        setValuesCampos(d);
        setEnabledCampos(b);

        boolean[] eb = { false, true, false, false, true };
        setEstadoBotones(eb);
        estado = Estados.CREANDO;
    }
    
    /**
     * Elimina un art�culo de la base de datos.
     */
    private void eliminarRegistro()
    {
        try
        {
            delegadoProcesos.eliminarArticulo(getArticulo());
            Mensajes.infoEliminar(MSJ_MODULO);
            limpiarCampos();
        }
        catch (Exception ex)
        {
            Mensajes.errorEliminar(MSJ_MODULO, ex);
        }
    }
    
    /**
     * Guarda un art�culo en la base de datos.
     */
    private void guardarRegistro()
    {
        boolean validos = Componentes.validarCampos(getComponentes(), getTipos());
        if (!validos)
        {
            Mensajes.camposVacios();
        }
        else
        {
            try
            { 
                Articulo articulo = getArticulo();
                
                if (estado == Estados.CREANDO)
                {
                    delegadoProcesos.guardarArticulo(articulo);
                }
                else if (estado == Estados.MODIFICANDO)
                {
                    delegadoProcesos.editarArticulo(articulo);
                }
                
                estado = Estados.CONSULTANDO;
                consultarDatos();
                Mensajes.infoActualizar(MSJ_MODULO);
            }
            catch (Exception ex)
            {
                Mensajes.errorActualizar(MSJ_MODULO, ex);
            }
        }
    }
    
    /**
     * Obtiene la entidad de art�culo para almacenar en la base de datos.
     * 
     * @return Art�culo para almacenar.
     */
    private Articulo getArticulo()
    {
        Articulo articulo = new Articulo();
        
        articulo.setCodigo(codigo.getValue().toString().toUpperCase());
        articulo.setNombre(nombre.getValue().toString().toUpperCase());
        articulo.setProveedor(codProv.getValue().toString().toUpperCase());
        articulo.setCaracteristicas(caracteristicas.getValue().toString().toUpperCase());
        
        return articulo;
    }
}
