/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.vistas.beans.procesos;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.lucasian.prueba.dto.procesos.FacturaDTO;
import com.lucasian.prueba.dto.procesos.FacturaDetalleDTO;
import com.lucasian.prueba.entidades.QuerysComp;
import com.lucasian.prueba.entidades.QuerysMod;
import com.lucasian.prueba.entidades.tablas.Factura;
import com.lucasian.prueba.utilidades.comunes.Componentes;
import com.lucasian.prueba.utilidades.comunes.Mensajes;
import com.lucasian.prueba.vistas.Campos;
import com.lucasian.prueba.vistas.Estados;
import com.lucasian.prueba.vistas.delegados.IDelegadoProcesos;

/**
 * Maneja los eventos y procesos realizados en el formulario de Facturas.
 * 
 * @author Joan Romero
 */
@ViewScoped
@ManagedBean
public class FacturasVista extends FacturasComp
{
    /**
     * Descripci�n para mostrar en los mensajes de formulario.
     */
    private static final String MSJ_MODULO = "la factura";
    
    /**
     * Estado actual en el formulario.
     */
    private Estados estado = Estados.CREANDO;
    
    /**
     * Delegado para realizar los procesos del formulario.
     */
    @ManagedProperty(value="#{delegadoProcesos}")
    private IDelegadoProcesos delegadoProcesos;
    
    /**
     * Genera una instancia para {@code FacturasVista}.
     */
    public FacturasVista()
    {
        super();
    }
    
    /**
     * Procesos necesarios al inicializar el formulario.
     */
    @PostConstruct
    public void init()
    {
        limpiarCampos();
    }

    /**
     * Obtiene el delegado para realizar los procesos del formulario.
     * 
     * @return Delegado para realizar los procesos del formulario.
     */
    public IDelegadoProcesos getDelegadoProcesos()
    {
        return delegadoProcesos;
    }

    /**
     * Fija el delegado para realizar los procesos del formulario.
     * 
     * @param delegadoProcesos Delegado para realizar los procesos del formulario.
     */
    public void setDelegadoProcesos(IDelegadoProcesos delegadoProcesos)
    {
        this.delegadoProcesos = delegadoProcesos;
    }
    
    /**
     * Evento al presionar el bot�n de Limpiar.
     */
    public void accionLimpiar()
    {
        limpiarCampos();
    }
    
    /**
     * Evento al presionar el bot�n de Guardar.
     */
    public void accionGuardar()
    {
        guardarRegistro();
    }
    
    /**
     * Evento al cambiar el valor del campo de C�digo.
     */
    public void codigoOnChange()
    {
        consultarDatos();
    }
    
    /**
     * Evento al cambiar el valor del campo de C�digo Cliente.
     */
    public void codClieOnChange()
    {
        consultarCliente();
    }
    
    /**
     * Evento al cambiar el valor del campo de C�digo Art�culo.
     */
    public void codArtOnChange()
    {
        consultarArticulo();
    }
    
    /**
     * Evento al presionar el bot�n de A�adir Art�culo.
     */
    public void accionAnadirArticulo()
    {
        anadirArticulo();
    }
    
    /**
     * Evento al presionar el bot�n de Remover Art�culo.
     */
    public void accionRemoverArticulo()
    {
        removerArticulo();
    }
    
    /**
     * Evento al seleccionar una fila de los Art�culos.
     */
    public void articulosOnRowSelect()
    {
        setValoresTablaArticulos();
    }
    
    /**
     * Consulta una factura.
     */
    public void consultarDatos()
    {
        try
        {
            if (estado != Estados.MODIFICANDO)
            {
                String num = numero.getValue().toString();
                if (!num.equals(""))
                {
                    Map<String, Object> param = new HashMap<>();
                    param.put("factura", Integer.parseInt(num));
                    
                    List<Object> data = delegadoProcesos.consultarEntidades(QuerysMod.PROC_FACTURAS, param,
                        "com.lucasian.prueba.dto.procesos.FacturaDTO");
                    
                    if (!data.isEmpty())
                    {
                        FacturaDTO f = (FacturaDTO)data.get(0);
                        
                        Object[] d = { f.getNumero(), f.getFecha(), f.getCifCliente(), f.getNomCliente() };
                        boolean[] b = { false, false, false };
                        
                        setValuesCampos(d);
                        setEnabledCampos(b);
                        
                        articulos.clear();
                        limpiarArticulo();
                        
                        List<Object> art = delegadoProcesos.consultarEntidades(QuerysMod.PROC_FACTURAS_DET,
                            param, "com.lucasian.prueba.dto.procesos.FacturaDetalleDTO");
                        
                        for (Object a : art)
                        {
                            FacturaDetalleDTO fd = (FacturaDetalleDTO)a;
                            articulos.add(fd);
                        }
                        
                        boolean[] ba = { false, false, false, false, false };
                        setEnabledArticulos(ba);
                        
                        
                        
                        boolean[] eb = { true, false };
                        setEstadoBotones(eb);
                        
                        estado = Estados.CONSULTANDO;
                    }
                    else
                    {
                        Mensajes.mostrarAdv("Advertencia", "La factura no se encuentra registrada");
                        limpiarCampos();
                    }
                }
                else
                {
                    Mensajes.mostrarAdv("Advertencia", "No hay ning�n n�mero de factura para consultar");
                    limpiarCampos();
                }
            }
            else
            {
                limpiarCampos();
            }
        }
        catch (Exception ex)
        {
            Mensajes.errorConsulta(ex);
            limpiarCampos();
        }
    }
    
    /**
     * Consulta un cliente.
     */
    private void consultarCliente()
    {
        try
        {
            String clie = codClie.getValue().toString();
            
            Map<String, Object> param = new HashMap<>();
            param.put("cliente", clie);
            
            List<Object[]> data = delegadoProcesos.consultarEntidades(QuerysComp.PROC_CLIENTE, param);
            
            Object[] comp = { codClie, nomClie };
            Campos[] tipoComp = { Campos.INPUTTEXT, Campos.INPUTTEXT };
            
            Componentes.setDatosConsultaExternos(estado, data, comp, tipoComp, 0);
            
            if (data.isEmpty())
            {
                Mensajes.mostrarAdv("Advertencia", "El cliente no se encuentra registrado");
            }
        }
        catch (Exception ex)
        {
            Mensajes.errorConsulta(ex);
        }
    }
    
    /**
     * Consulta un articulo.
     */
    private void consultarArticulo()
    {
        try
        {
            String art = codArt.getValue().toString();
            
            Map<String, Object> param = new HashMap<>();
            param.put("articulo", art);
            
            List<Object[]> data = delegadoProcesos.consultarEntidades(QuerysComp.PROC_ARTICULO, param);
            
            Object[] comp = { codArt, nomArt };
            Campos[] tipoComp = { Campos.INPUTTEXT, Campos.INPUTTEXT };
            
            Componentes.setDatosConsultaExternos(estado, data, comp, tipoComp, 0);
            
            if (data.isEmpty())
            {
                Mensajes.mostrarAdv("Advertencia", "El art�culo no se encuentra registrado");
            }
        }
        catch (Exception ex)
        {
            Mensajes.errorConsulta(ex);
        }
    }
    
    /**
     * Fija el n�mero de la siguiente factura.
     */
    private void setNumeroFactura()
    {
        try
        {
            int fac = delegadoProcesos.getNumeroFactura();
            numero.setValue(String.valueOf(fac));
        }
        catch (Exception ex)
        {
            Mensajes.errorConsulta(ex);
        }
    }
    
    /**
     * Valida y agrega un art�culo a la factura.
     */
    private void anadirArticulo()
    {
        if (!Componentes.validarCampos(getComponentesArticulo(), getTiposArticulo()))
        {
            Mensajes.camposVacios("el art�culo");
        }
        else
        {
            int cant = Integer.parseInt(cantidad.getValue().toString());
            int pre = Integer.parseInt(precio.getValue().toString());
            
            if (cant == 0)
            {
                Mensajes.mostrarAdv("Advertencia", "La cantidad debe ser mayor a cero");
                return;
            }
            
            FacturaDetalleDTO a = new FacturaDetalleDTO();
            
            a.setDetalle(Integer.parseInt(detalle.getValue().toString()));
            a.setCodArticulo(codArt.getValue().toString().toUpperCase());
            a.setNomArticulo(nomArt.getValue().toString().toUpperCase());
            a.setCantidad(cant);
            a.setPrecio(pre);
            
            int row = articulos.indexOf(selArticulo);

            if (row == -1)
            {
                articulos.add(a);
                limpiarArticulo();
            }
            else
            {
                articulos.set(row, a);
                limpiarArticulo();
            }
        }
    }
    
    /**
     * Retira un art�culo de la factura.
     */
    private void removerArticulo()
    {
        if (selArticulo == null)
        {
            Mensajes.mostrarAdv("Advertencia", "Debe seleccionar un art�culo para eliminar");
        }
        else
        {
            articulos.remove(selArticulo);
            limpiarArticulo();
        }
    }
    
    /**
     * Limpia los campos del art�culo.
     */
    private void limpiarArticulo()
    {
        Object[] d = { "", "", "", "", "", "" };
        setValuesArticulos(d);
        selArticulo = null;
        setDetalleFactura();
    }
    
    /**
     * Fija el detalle del art�culo siguiente para la factura.
     */
    private void setDetalleFactura()
    {
        int det = delegadoProcesos.getDetalleFactura(articulos);
        detalle.setValue(String.valueOf(det));
    }
    
    /**
     * Fija los valores de un articulo en el formulario.
     */
    private void setValoresTablaArticulos()
    {
        if (estado == Estados.CREANDO || estado == Estados.MODIFICANDO)
        {
            if (selArticulo != null)
            {                
                Object[] d = { selArticulo.getDetalle(), selArticulo.getCodArticulo(),
                    selArticulo.getNomArticulo(), selArticulo.getCantidad(), selArticulo.getPrecio() };
                setValuesArticulos(d);
            }
        }
    }
    
    /**
     * Limpia los campos del formulario y establece el estado de los botones.
     */
    private void limpiarCampos()
    {
        Object[] d = { "", "", "", "" };
        boolean[] b = { true, true, true };
        
        setValuesCampos(d);
        setEnabledCampos(b);
        
        articulos.clear();
        limpiarArticulo();
        boolean[] ba = { true, true, true, true, true };
        setEnabledArticulos(ba);
        
        setNumeroFactura();

        boolean[] eb = { true, true };
        setEstadoBotones(eb);
        estado = Estados.CREANDO;
    }
    
    /**
     * Guarda una factura en la base de datos.
     */
    private void guardarRegistro()
    {
        boolean validos = Componentes.validarCampos(getComponentes(), getTipos());
        if (!validos)
        {
            Mensajes.camposVacios();
        }
        else if (articulos.isEmpty())
        {
            Mensajes.mostrarAdv("Advertencia", "Debe ingresar al menos un art�culo para la factura");
        }
        else
        {
            try
            { 
                Factura factura = getFactura();
                
                if (estado == Estados.CREANDO)
                {
                    delegadoProcesos.guardarFactura(factura, articulos);
                }
                else if (estado == Estados.MODIFICANDO)
                {
                    throw new Exception("No puede modificar la factura");
                }
                
                limpiarCampos();
                Mensajes.infoActualizar(MSJ_MODULO);
            }
            catch (Exception ex)
            {
                Mensajes.errorActualizar(MSJ_MODULO, ex);
            }
        }
    }
    
    /**
     * Obtiene la entidad de factura para almacenar en la base de datos.
     * 
     * @return Factura para almacenar.
     */
    private Factura getFactura()
    {
        Factura factura = new Factura();
        
        factura.setNumero(Integer.parseInt(numero.getValue().toString()));
        factura.setFecha((Date)fecha.getValue());
        factura.setCliente(codClie.getValue().toString().toUpperCase());
        
        return factura;
    }
}
