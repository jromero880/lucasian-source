/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.vistas.beans.procesos;

import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;

import com.lucasian.prueba.utilidades.comunes.Datos;
import com.lucasian.prueba.vistas.Campos;

/**
 * Define los componentes y m�todos generales a ser utilizados en el formulario de Clientes.
 * 
 * @author Joan Romero
 */
public class ClientesComp
{
    protected InputText codigo;
    protected InputText nombre;
    protected InputText apellido1;
    protected InputText apellido2;
    protected InputText empresa;
    protected InputText direccion;
    protected InputText ciudad;
    protected InputText cp;
    protected InputText provincia;
    protected InputText pais;
    
    protected CommandButton modificar;
    protected CommandButton limpiar;
    protected CommandButton cancelar;
    protected CommandButton eliminar;
    protected CommandButton guardar;
    
    /**
     * Genera una instancia para {@code ClientesComp}.
     */
    public ClientesComp()
    {
        codigo = new InputText();
        nombre = new InputText();
        apellido1 = new InputText();
        apellido2 = new InputText();
        empresa = new InputText();
        direccion = new InputText();
        ciudad = new InputText();
        cp = new InputText();
        provincia = new InputText();
        pais = new InputText();
        
        inicializarBotones();
    }
    
    public InputText getCodigo()
    {
        return codigo;
    }

    public void setCodigo(InputText codigo)
    {
        this.codigo = codigo;
    }

    public InputText getNombre()
    {
        return nombre;
    }

    public void setNombre(InputText nombre)
    {
        this.nombre = nombre;
    }

    public InputText getApellido1()
    {
        return apellido1;
    }

    public void setApellido1(InputText apellido1)
    {
        this.apellido1 = apellido1;
    }

    public InputText getApellido2()
    {
        return apellido2;
    }

    public void setApellido2(InputText apellido2)
    {
        this.apellido2 = apellido2;
    }

    public InputText getEmpresa()
    {
        return empresa;
    }

    public void setEmpresa(InputText empresa)
    {
        this.empresa = empresa;
    }

    public InputText getDireccion()
    {
        return direccion;
    }

    public void setDireccion(InputText direccion)
    {
        this.direccion = direccion;
    }

    public InputText getCiudad()
    {
        return ciudad;
    }

    public void setCiudad(InputText ciudad)
    {
        this.ciudad = ciudad;
    }

    public InputText getCp()
    {
        return cp;
    }

    public void setCp(InputText cp)
    {
        this.cp = cp;
    }

    public InputText getProvincia()
    {
        return provincia;
    }

    public void setProvincia(InputText provincia)
    {
        this.provincia = provincia;
    }

    public InputText getPais()
    {
        return pais;
    }

    public void setPais(InputText pais)
    {
        this.pais = pais;
    }

    public CommandButton getModificar()
    {
        return modificar;
    }

    public void setModificar(CommandButton modificar)
    {
        this.modificar = modificar;
    }

    public CommandButton getLimpiar()
    {
        return limpiar;
    }

    public void setLimpiar(CommandButton limpiar)
    {
        this.limpiar = limpiar;
    }

    public CommandButton getCancelar()
    {
        return cancelar;
    }

    public void setCancelar(CommandButton cancelar)
    {
        this.cancelar = cancelar;
    }

    public CommandButton getEliminar()
    {
        return eliminar;
    }

    public void setEliminar(CommandButton eliminar)
    {
        this.eliminar = eliminar;
    }

    public CommandButton getGuardar()
    {
        return guardar;
    }

    public void setGuardar(CommandButton guardar)
    {
        this.guardar = guardar;
    }

    /**
     * Instancia los botones y establece su estado por defecto.
     */
    protected void inicializarBotones()
    {
        modificar = new CommandButton();
        limpiar = new CommandButton();
        cancelar = new CommandButton();
        eliminar = new CommandButton();
        guardar = new CommandButton();
        
        boolean[] eb = { false, true, false, false, true };
        setEstadoBotones(eb);
    }
    
    /**
     * Establece los valores para los componentes del formulario.
     *
     * @param d Valores para los componentes.
     */
    protected void setValuesCampos(Object[] d)
    {
        d = Datos.cambiarNull(d);
        
        codigo.setValue(d[0]);
        nombre.setValue(d[1]);
        apellido1.setValue(d[2]);
        apellido2.setValue(d[3]);
        empresa.setValue(d[4]);
        direccion.setValue(d[5]);
        ciudad.setValue(d[6]);
        cp.setValue(d[7]);
        provincia.setValue(d[8]);
        pais.setValue(d[9]);
    }
    
    /**
     * Establece si se habilitan o no los componentes dentro del formulario.
     *
     * @param b Estados para los componentes.
     */
    protected void setEnabledCampos(boolean[] b)
    {
        codigo.setReadonly(!b[0]);
        nombre.setReadonly(!b[1]);
        apellido1.setReadonly(!b[2]);
        apellido2.setReadonly(!b[3]);
        empresa.setReadonly(!b[4]);
        direccion.setReadonly(!b[5]);
        ciudad.setReadonly(!b[6]);
        cp.setReadonly(!b[7]);
        provincia.setReadonly(!b[8]);
        pais.setReadonly(!b[9]);
    }
    
    /**
     * Establece si se habilitan o no los botones dentro del formulario.
     *
     * @param b Estados para los botones.
     */
    protected void setEstadoBotones(boolean[] b)
    {
        modificar.setDisabled(!b[0]);
        limpiar.setDisabled(!b[1]);
        cancelar.setDisabled(!b[2]);
        eliminar.setDisabled(!b[3]);
        guardar.setDisabled(!b[4]);
    }
    
    /**
     * Obtiene los componentes que son obligatorios en el formulario.
     * 
     * @return Lista de componentes obligatorios.
     */
    protected Object[] getComponentes()
    {
        return new Object[] { codigo, nombre, apellido1, empresa, direccion, ciudad, cp, provincia, pais };
    }
    
    /**
     * Obtiene los tipos de componentes que son obligatorios en el formulario.
     * 
     * @return Lista de tipos de componentes.
     */
    protected Campos[] getTipos()
    {
        return new Campos[] { Campos.INPUTTEXT, Campos.INPUTTEXT, Campos.INPUTTEXT, Campos.INPUTTEXT,
            Campos.INPUTTEXT, Campos.INPUTTEXT, Campos.INPUTTEXT, Campos.INPUTTEXT, Campos.INPUTTEXT };
    }
}
