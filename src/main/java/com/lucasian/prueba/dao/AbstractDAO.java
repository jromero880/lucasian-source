/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.dao;

import java.util.List;
import java.util.Map;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

/**
 * Implementa la interfaz {@code IAbstractDAO} soportando las operaciones de guardado, modificación, 
 * eliminación, consulta y ejecución de sentencias para entidades genericas.
 * 
 * @author Joan Romero
 */
@Repository
@Scope("singleton")
public class AbstractDAO implements IAbstractDAO
{
    /**
     * Objeto que obtiene la sesión y ejecuta la operación en la base de datos.
     */
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public String guardarEntidad(Object entidad) throws Exception
    {
        sessionFactory.getCurrentSession().save(entidad);
        return "ejecutado";
    }

    @Override
    public String editarEntidad(Object entidad) throws Exception
    {
        sessionFactory.getCurrentSession().update(entidad);
        return "ejecutado";
    }

    @Override
    public String guardarEditarEntidad(Object entidad) throws Exception
    {
        sessionFactory.getCurrentSession().saveOrUpdate(entidad);
        return "ejecutado";
    }

    @Override
    public String eliminarEntidad(Object entidad) throws Exception
    {
        sessionFactory.getCurrentSession().delete(entidad);
        return "ejecutado";
    }

    @Override
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public List<Object[]> consultarEntidades(String namedQuery, Map<String, Object> parametros)
            throws Exception
    {
        List<Object[]> datos = null;
        Query<Object[]> query = sessionFactory.getCurrentSession().getNamedQuery(namedQuery);

        for (String param : parametros.keySet())
        {
            if (parametros.get(param) instanceof List)
            {
                query.setParameterList(param, (List) parametros.get(param));
            }
            else
            {
                query.setParameter(param, parametros.get(param));
            }
        }

        datos = query.list();
        return datos;
    }

    @Override
    @SuppressWarnings({ "unchecked", "deprecation", "rawtypes" })
    public List<Object> consultarEntidades(String namedQuery, Map<String, Object> parametros, String clase)
            throws Exception
    {
        List<Object> datos = null;
        Query<Object> query = sessionFactory.getCurrentSession().getNamedQuery(namedQuery);
        query.setResultTransformer(Transformers.aliasToBean(Class.forName(clase)));

        for (String param : parametros.keySet())
        {
            if (parametros.get(param) instanceof List)
            {
                query.setParameterList(param, (List) parametros.get(param));
            }
            else
            {
                query.setParameter(param, parametros.get(param));
            }
        }

        datos = query.list();
        return datos;
    }

    @Override
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public Object consultarRegistro(String namedQuery, Map<String, Object> parametros) throws Exception
    {
        Object dato = null;
        Query<Object> query = sessionFactory.getCurrentSession().getNamedQuery(namedQuery);

        for (String param : parametros.keySet())
        {
            if (parametros.get(param) instanceof List)
            {
                query.setParameterList(param, (List) parametros.get(param));
            }
            else
            {
                query.setParameter(param, parametros.get(param));
            }
        }

        dato = query.uniqueResult();
        return dato;
    }

    @Override
    @SuppressWarnings("rawtypes")
    public String ejecutarSentencia(String namedQuery, Map<String, Object> parametros) throws Exception
    {
        Query query = sessionFactory.getCurrentSession().getNamedQuery(namedQuery);

        for (String param : parametros.keySet())
        {
            if (parametros.get(param) instanceof List)
            {
                query.setParameterList(param, (List) parametros.get(param));
            }
            else
            {
                query.setParameter(param, parametros.get(param));
            }
        }

        query.executeUpdate();
        return "ejecutado";
    }
}
