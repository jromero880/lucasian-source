/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.dto.consultas;

/**
 * Clase para obtener registros de consulta de base de datos. Mapea los datos de consultas de informes de
 * facturas.
 * 
 * @author Joan Romero
 */
public class InformeFacturasDTO
{
    private String cifCliente;
    private String nomCliente;
    private int total;

    public String getCifCliente()
    {
        return cifCliente;
    }

    public void setCifCliente(String cifCliente)
    {
        this.cifCliente = cifCliente;
    }

    public String getNomCliente()
    {
        return nomCliente;
    }

    public void setNomCliente(String nomCliente)
    {
        this.nomCliente = nomCliente;
    }

    public int getTotal()
    {
        return total;
    }

    public void setTotal(int total)
    {
        this.total = total;
    }
}
