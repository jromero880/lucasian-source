/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.dto.procesos;

/**
 * Clase para obtener registros de consulta de base de datos. Mapea los datos de detalle de factura.
 * 
 * @author Joan Romero
 */
public class FacturaDetalleDTO
{
    private int detalle;
    private String codArticulo;
    private String nomArticulo;
    private int cantidad;
    private int precio;

    public int getDetalle()
    {
        return detalle;
    }

    public void setDetalle(int detalle)
    {
        this.detalle = detalle;
    }

    public String getCodArticulo()
    {
        return codArticulo;
    }

    public void setCodArticulo(String codArticulo)
    {
        this.codArticulo = codArticulo;
    }

    public String getNomArticulo()
    {
        return nomArticulo;
    }

    public void setNomArticulo(String nomArticulo)
    {
        this.nomArticulo = nomArticulo;
    }

    public int getCantidad()
    {
        return cantidad;
    }

    public void setCantidad(int cantidad)
    {
        this.cantidad = cantidad;
    }

    public int getPrecio()
    {
        return precio;
    }

    public void setPrecio(int precio)
    {
        this.precio = precio;
    }
}
