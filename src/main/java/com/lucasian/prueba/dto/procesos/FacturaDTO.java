/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.dto.procesos;

import java.util.Date;

/**
 * Clase para obtener registros de consulta de base de datos. Mapea los datos de factura.
 * 
 * @author Joan Romero
 */
public class FacturaDTO
{
    private int numero;
    private Date fecha;
    private String cifCliente;
    private String nomCliente;

    public int getNumero()
    {
        return numero;
    }

    public void setNumero(int numero)
    {
        this.numero = numero;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public String getCifCliente()
    {
        return cifCliente;
    }

    public void setCifCliente(String cifCliente)
    {
        this.cifCliente = cifCliente;
    }

    public String getNomCliente()
    {
        return nomCliente;
    }

    public void setNomCliente(String nomCliente)
    {
        this.nomCliente = nomCliente;
    }
}
