/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.logica;

import java.util.List;
import java.util.Map;

/**
 * Interface que provee todos los metodos necesarios para hacer operaciones en la base de datos. Soporta
 * las siguientes operaciones:
 * <ul>
 * <li>Guardar Entidades
 * <li>Modificar Entidades
 * <li>Eliminar Entidades
 * <li>Consultar Entidades
 * <li>Consultar Registros
 * <li>Ejecutar Sentencias
 * </ul>
 * 
 * @author Joan Romero
 * 
 */
public interface ILogicaAbstract
{
    /**
     * Guarda en la base de datos una entidad respectivamente mapeada.
     * 
     * @param entidad Registro a ser almacenado
     * @return Estado del proceso de almacenamiento
     * @throws Exception Si se presenta algun error en el proceso de almacenamiento
     */
    public String guardarEntidad(Object entidad) throws Exception;

    /**
     * Edita en la base de datos una entidad respectivamente mapeada.
     * 
     * @param entidad Registro a ser modificado
     * @return Estado del proceso de modificacion
     * @throws Exception Si se presenta algun error en el proceso de modificacion
     */
    public String editarEntidad(Object entidad) throws Exception;

    /**
     * Guarda o edita en la base de datos una entidad respectivamente mapeada.
     * <p>
     * En caso de no existir la entidad la almacena, de caso contrario la modifica.
     * 
     * @param entidad Registro a ser almacenado o modificado
     * @return Estado del proceso
     * @throws Exception Si se presenta algun error en el proceso de almacenamiento o modificacion
     */
    public String guardarEditarEntidad(Object entidad) throws Exception;

    /**
     * Elimina de la base de datos una entidad respectivamente mapeada.
     * 
     * @param entidad Registro a ser eliminado
     * @return Estado del proceso de eliminacion
     * @throws Exception Si se presenta algun error en el proceso de borrado
     */
    public String eliminarEntidad(Object entidad) throws Exception;

    /**
     * Consulta registros de la base de datos con base a un query de los que se encuentran mapeados.
     * 
     * @param namedQuery Nombre del query
     * @param parametros Parametros de la consulta
     * @return Listado con cada uno de los registros encontrados. Estos se encuentran en un array generico
     * de tipo {@code Object}
     * @throws Exception Si se presenta algun error al establecer parametros o realizar la consulta
     * en la base de datos. Puede presentarse tambien si el query suministrado no se encuentra en
     * los archivos mapeados
     */
    public List<Object[]> consultarEntidades(String namedQuery, Map<String, Object> parametros)
            throws Exception;

    /**
     * Consulta registros de la base de datos con base a un query de los que se encuentran mapeados.
     * 
     * @param namedQuery Nombre del query
     * @param parametros Parametros de la consulta
     * @param clase Clase en la cual se van a mapear los registros encontrados. Corresponde al paquete
     * y nombre de la clase tipo {@code com.project.Class} en forma de cadena
     * @return Listado con cada uno de los registros encontrados. Estos se encuentran en un objeto generico
     * de tipo {@code Object} que puede ser convertido en la clase suministrada
     * @throws Exception Si se presenta algun error al establecer parametros o realizar la consulta
     * en la base de datos. Puede presentarse tambien si el query suministrado no se encuentra en
     * los archivos mapeados o si no pueden mapearse los resultados en la clase dada 
     */
    public List<Object> consultarEntidades(String namedQuery, Map<String, Object> parametros, String clase)
            throws Exception;

    /**
     * Consulta un unico campo de la base de datos con base a un query de los que se encuentran mapeados.
     * 
     * @param namedQuery Nombre del query
     * @param parametros Parametros de la consulta
     * @return Valor del campo recuperado
     * @throws Exception Si se presenta algun error al establecer parametros o realizar la consulta
     * en la base de datos. Puede presentarse tambien si el query suministrado no se encuentra en
     * los archivos mapeados o si la consulta devuelve mas de un campo o registro 
     */
    public Object consultarRegistro(String namedQuery, Map<String, Object> parametros) throws Exception;

    /**
     * Ejecuta una sentencia en la base de datos con base a un query de los que se encuentran mapeados.
     * 
     * @param namedQuery Nombre del query
     * @param parametros Parametros de la sentencia
     * @return Estado del proceso de ejecucion
     * @throws Exception Si se presenta algun error al establecer parametros o ejecutar la sentencia
     * en la base de datos. Puede presentarse tambien si el query suministrado no se encuentra en
     * los archivos mapeados
     */
    public String ejecutarSentencia(String namedQuery, Map<String, Object> parametros) throws Exception;
}
