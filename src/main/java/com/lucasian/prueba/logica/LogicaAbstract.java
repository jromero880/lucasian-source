/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.logica;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.lucasian.prueba.dao.IAbstractDAO;

/**
 * Implementacion de la interface {@code ILogicaAbstract} para realizar las respectivas operaciones
 * soportadas en la base de datos.
 * 
 * @author Joan Romero
 *
 */
@Repository
@Scope("singleton")
public class LogicaAbstract implements ILogicaAbstract
{
    /**
     * Objeto para el manejo de transacciones con la base de datos.
     */
    @Autowired
    private IAbstractDAO abstractDAO;
    
    @Override
    public String guardarEntidad(Object entidad) throws Exception
    {
        return abstractDAO.guardarEntidad(entidad);
    }

    @Override
    public String editarEntidad(Object entidad) throws Exception
    {
        return abstractDAO.editarEntidad(entidad);
    }

    @Override
    public String guardarEditarEntidad(Object entidad) throws Exception
    {
        return abstractDAO.guardarEditarEntidad(entidad);
    }

    @Override
    public String eliminarEntidad(Object entidad) throws Exception
    {
        return abstractDAO.eliminarEntidad(entidad);
    }

    @Override
    public List<Object[]> consultarEntidades(String namedQuery, Map<String, Object> parametros)
            throws Exception
    {
        return abstractDAO.consultarEntidades(namedQuery, parametros);
    }

    @Override
    public List<Object> consultarEntidades(String namedQuery, Map<String, Object> parametros, String clase)
            throws Exception
    {
        return abstractDAO.consultarEntidades(namedQuery, parametros, clase);
    }

    @Override
    public Object consultarRegistro(String namedQuery, Map<String, Object> parametros) throws Exception
    {
        return abstractDAO.consultarRegistro(namedQuery, parametros);
    }

    @Override
    public String ejecutarSentencia(String namedQuery, Map<String, Object> parametros) throws Exception
    {
        return abstractDAO.ejecutarSentencia(namedQuery, parametros);
    }
}
