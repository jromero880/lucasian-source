/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.logica.procesos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.OptionalInt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.lucasian.prueba.dto.procesos.FacturaDetalleDTO;
import com.lucasian.prueba.entidades.QuerysMod;
import com.lucasian.prueba.entidades.tablas.Factura;
import com.lucasian.prueba.entidades.tablas.FacturaDetalle;
import com.lucasian.prueba.logica.ILogicaAbstract;

/**
 * Implementa la interfaz {@code ILogicaFactura} con todos los procesos soportados para manejo de facturas.
 * 
 * @author Joan Romero
 */
@Repository
@Scope("singleton")
public class LogicaFactura implements ILogicaFactura
{
    /**
     * L�gica para el manejo de transacciones con la base de datos.
     */
    @Autowired
    private ILogicaAbstract logicaAbstract;
    
    @Override
    @Transactional(readOnly = true)
    public int getNumeroFactura() throws Exception
    {
        Object oFac = logicaAbstract.consultarRegistro(QuerysMod.PROC_NUM_FACTURA, new HashMap<>());
        int fac = oFac == null ? 0 : Integer.parseInt(oFac.toString());
        fac++;
        
        return fac;
    }

    @Override
    public int getDetalleFactura(List<FacturaDetalleDTO> articulos)
    {
        int reg = 1;
        if (!articulos.isEmpty())
        {
            OptionalInt max = articulos.stream().mapToInt(FacturaDetalleDTO::getDetalle).max();
            reg = max.getAsInt() + 1;
        }
        
        return reg;
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public String guardarFactura(Factura factura, List<FacturaDetalleDTO> articulos) throws Exception
    {
        logicaAbstract.guardarEntidad(factura);
        procesarArticulos(factura.getNumero(), articulos);
        
        return "ejecutado";
    }
    
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public String eliminarFactura(int numero) throws Exception
    {
        Map<String, Object> param = new HashMap<>();
        param.put("factura", numero);
        
        logicaAbstract.ejecutarSentencia(QuerysMod.PRUE_ELIM_DET_FACTURA, param);
        logicaAbstract.ejecutarSentencia(QuerysMod.PRUE_ELIM_FACTURA, param);
        
        return "ejecutado";
    }
    
    /**
     * Procesa los art�culos y los almacena en la base de datos.
     * 
     * @param numero N�mero de la factura.
     * @param articulos Listado de art�culos de la factura.
     * @throws Exception Si se presenta algun error en el proceso.
     */
    private void procesarArticulos(int numero, List<FacturaDetalleDTO> articulos) throws Exception
    {
        for (FacturaDetalleDTO a : articulos)
        {
            FacturaDetalle articulo = getArticuloFactura(numero, a);
            logicaAbstract.guardarEntidad(articulo);
        }
    }
    
    /**
     * Obtiene la entidad con los datos del art�culo.
     * 
     * @param numero N�mero de la factura.
     * @param a Datos del art�culo.
     * @return Entidad con los datos del art�culo.
     */
    private FacturaDetalle getArticuloFactura(int numero, FacturaDetalleDTO a)
    {
        FacturaDetalle articulo = new FacturaDetalle();
        
        articulo.setNumero(numero);
        articulo.setDetalle(a.getDetalle());
        articulo.setArticulo(a.getCodArticulo());
        articulo.setCantidad(a.getCantidad());
        articulo.setPrecio(a.getPrecio());
        
        return articulo;
    }
}
