/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.logica.procesos;

import com.lucasian.prueba.entidades.tablas.Cliente;

/**
 * Interface encargada de manejar la logica de las operaciones realizadas con los clientes.
 * 
 * @author Joan Romero
 */
public interface ILogicaCliente
{
    /**
     * Guarda los datos de un cliente nuevo.
     * 
     * @param cliente Entidad con los datos del cliente.
     * @return Estado del proceso, siendo {@code ejecutado} cuando es correcto.
     * @throws Exception Si se presenta algun error en el proceso.
     */
    public String guardarCliente(Cliente cliente) throws Exception;
    
    /**
     * Modifica los datos de un cliente registrado.
     * 
     * @param cliente Entidad con los datos del cliente.
     * @return Estado del proceso, siendo {@code ejecutado} cuando es correcto.
     * @throws Exception Si se presenta algun error en el proceso.
     */
    public String editarCliente(Cliente cliente) throws Exception;
    
    /**
     * Elimina un cliente registrado.
     * 
     * @param cliente Entidad con los datos del cliente.
     * @return Estado del proceso, siendo {@code ejecutado} cuando es correcto.
     * @throws Exception Si se presenta algun error en el proceso.
     */
    public String eliminarCliente(Cliente cliente) throws Exception;
}
