/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.logica.procesos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.lucasian.prueba.entidades.tablas.Articulo;
import com.lucasian.prueba.logica.ILogicaAbstract;

/**
 * Implementa la interfaz {@code ILogicaArticulo} con todos los procesos soportados para manejo de art�culos.
 * 
 * @author Joan Romero
 */
@Repository
@Scope("singleton")
public class LogicaArticulo implements ILogicaArticulo
{
    /**
     * L�gica para el manejo de transacciones con la base de datos.
     */
    @Autowired
    private ILogicaAbstract logicaAbstract;
    
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public String guardarArticulo(Articulo articulo) throws Exception
    {
        return logicaAbstract.guardarEntidad(articulo);
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public String editarArticulo(Articulo articulo) throws Exception
    {
        return logicaAbstract.editarEntidad(articulo);
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public String eliminarArticulo(Articulo articulo) throws Exception
    {
        return logicaAbstract.eliminarEntidad(articulo);
    }

}
