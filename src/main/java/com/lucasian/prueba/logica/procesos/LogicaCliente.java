/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.logica.procesos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.lucasian.prueba.entidades.tablas.Cliente;
import com.lucasian.prueba.logica.ILogicaAbstract;

/**
 * Implementa la interfaz {@code ILogicaCliente} con todos los procesos soportados para manejo de clientes.
 * 
 * @author Joan Romero
 */
@Repository
@Scope("singleton")
public class LogicaCliente implements ILogicaCliente
{
    /**
     * L�gica para el manejo de transacciones con la base de datos.
     */
    @Autowired
    private ILogicaAbstract logicaAbstract;
    
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public String guardarCliente(Cliente cliente) throws Exception
    {
        return logicaAbstract.guardarEntidad(cliente);
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public String editarCliente(Cliente cliente) throws Exception
    {
        return logicaAbstract.editarEntidad(cliente);
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public String eliminarCliente(Cliente cliente) throws Exception
    {
        return logicaAbstract.eliminarEntidad(cliente);
    }

}
