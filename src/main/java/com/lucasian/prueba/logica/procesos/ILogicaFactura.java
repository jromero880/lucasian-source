/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.logica.procesos;

import java.util.List;

import com.lucasian.prueba.dto.procesos.FacturaDetalleDTO;
import com.lucasian.prueba.entidades.tablas.Factura;

/**
 * Interface encargada de manejar la logica de las operaciones realizadas con las facturas.
 * 
 * @author Joan Romero
 */
public interface ILogicaFactura
{
    /**
     * Obtiene el n�mero de la siguiente factura.
     * 
     * @return N�mero de la factura.
     * @throws Exception Si se presenta algun error en el proceso.
     */
    public int getNumeroFactura() throws Exception;
    
    /**
     * Obtiene el n�mero del siguiente art�culo de la factura.
     * 
     * @param articulos Listado de art�culos de la factura.
     * @return N�mero del art�culo.
     */
    public int getDetalleFactura(List<FacturaDetalleDTO> articulos);
    
    /**
     * Guarda los datos de una factura nueva.
     * 
     * @param factura Entidad con los datos de la factura.
     * @param articulos Listado de art�culos de la factura.
     * @return Estado del proceso, siendo {@code ejecutado} cuando es correcto.
     * @throws Exception Si se presenta algun error en el proceso.
     */
    public String guardarFactura(Factura factura, List<FacturaDetalleDTO> articulos) throws Exception;
    
    /**
     * Elimina una factura con su detalle de la base de datos.
     * 
     * @param numero N�mero de la factura.
     * @return Estado del proceso, siendo {@code ejecutado} cuando es correcto.
     * @throws Exception Si se presenta algun error en el proceso.
     */
    public String eliminarFactura(int numero) throws Exception;
}
