/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.logica.procesos;

import com.lucasian.prueba.entidades.tablas.Proveedor;

/**
 * Interface encargada de manejar la logica de las operaciones realizadas con los proveedores.
 * 
 * @author Joan Romero
 */
public interface ILogicaProveedor
{
    /**
     * Guarda los datos de un proveedor nuevo.
     * 
     * @param proveedor Entidad con los datos del proveedor.
     * @return Estado del proceso, siendo {@code ejecutado} cuando es correcto.
     * @throws Exception Si se presenta algun error en el proceso.
     */
    public String guardarProveedor(Proveedor proveedor) throws Exception;
    
    /**
     * Modifica los datos de un proveedor registrado.
     * 
     * @param proveedor Entidad con los datos del proveedor.
     * @return Estado del proceso, siendo {@code ejecutado} cuando es correcto.
     * @throws Exception Si se presenta algun error en el proceso.
     */
    public String editarProveedor(Proveedor proveedor) throws Exception;
    
    /**
     * Elimina un proveedor registrado.
     * 
     * @param proveedor Entidad con los datos del proveedor.
     * @return Estado del proceso, siendo {@code ejecutado} cuando es correcto.
     * @throws Exception Si se presenta algun error en el proceso.
     */
    public String eliminarProveedor(Proveedor proveedor) throws Exception;
}
