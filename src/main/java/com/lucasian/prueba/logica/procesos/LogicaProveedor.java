/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.logica.procesos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.lucasian.prueba.entidades.tablas.Proveedor;
import com.lucasian.prueba.logica.ILogicaAbstract;

/**
 * Implementa la interfaz {@code ILogicaProveedor} con todos los procesos soportados para manejo de
 * proveedores.
 * 
 * @author Joan Romero
 */
@Repository
@Scope("singleton")
public class LogicaProveedor implements ILogicaProveedor
{
    /**
     * L�gica para el manejo de transacciones con la base de datos.
     */
    @Autowired
    private ILogicaAbstract logicaAbstract;
    
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public String guardarProveedor(Proveedor proveedor) throws Exception
    {
        return logicaAbstract.guardarEntidad(proveedor);
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public String editarProveedor(Proveedor proveedor) throws Exception
    {
        return logicaAbstract.editarEntidad(proveedor);
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public String eliminarProveedor(Proveedor proveedor) throws Exception
    {
        return logicaAbstract.eliminarEntidad(proveedor);
    }

}
