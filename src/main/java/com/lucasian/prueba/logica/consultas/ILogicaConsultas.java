/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.logica.consultas;

import java.util.List;
import java.util.Map;

/**
 * Interface encargada de manejar la logica de las operaciones relacionadas con consultas en base de datos.
 * 
 * @author Joan Romero
 */
public interface ILogicaConsultas
{
    /**
     * Consulta registros de la base de datos con base a un query de los que se encuentran mapeados.
     * 
     * @param namedQuery Nombre del query.
     * @param parametros Parametros de la consulta.
     * @return Listado con cada uno de los registros encontrados. Estos se encuentran en un array generico
     * de tipo {@code Object}.
     * @throws Exception Si se presenta algun error al establecer parametros o realizar la consulta
     * en la base de datos. Puede presentarse tambien si el query suministrado no se encuentra en
     * los archivos mapeados.
     */
    public List<Object[]> consultarEntidades(String namedQuery, Map<String, Object> parametros)
            throws Exception;

    /**
     * Consulta registros de la base de datos con base a un query de los que se encuentran mapeados.
     * 
     * @param namedQuery Nombre del query.
     * @param parametros Parametros de la consulta.
     * @param clase Clase en la cual se van a mapear los registros encontrados. Corresponde al paquete
     * y nombre de la clase tipo {@code com.project.Class} en forma de cadena.
     * @return Listado con cada uno de los registros encontrados. Estos se encuentran en un objeto generico
     * de tipo {@code Object} que puede ser convertido en la clase suministrada.
     * @throws Exception Si se presenta algun error al establecer parametros o realizar la consulta
     * en la base de datos. Puede presentarse tambien si el query suministrado no se encuentra en
     * los archivos mapeados o si no pueden mapearse los resultados en la clase dada.
     */
    public List<Object> consultarEntidades(String namedQuery, Map<String, Object> parametros, String clase)
            throws Exception;
    
    /**
     * Consulta un unico campo de la base de datos con base a un query de los que se encuentran mapeados.
     * 
     * @param namedQuery Nombre del query.
     * @param parametros Parametros de la consulta.
     * @return Valor del campo recuperado.
     * @throws Exception Si se presenta algun error al establecer parametros o realizar la consulta
     * en la base de datos. Puede presentarse tambien si el query suministrado no se encuentra en
     * los archivos mapeados o si la consulta devuelve mas de un campo o registro.
     */
    public Object consultarRegistro(String namedQuery, Map<String, Object> parametros) throws Exception;
}
