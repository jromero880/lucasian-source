/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.logica.consultas;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lucasian.prueba.logica.ILogicaAbstract;

/**
 * Implementa la interfaz {@code ILogicaConsultas} con todos las operaciones soportadas para consultas en 
 * base de datos. Todas las transacciones de estas operaciones son de solo lectura ({@code readOnly = true}).
 * 
 * @author Joan Romero
 */
@Repository
@Scope("singleton")
public class LogicaConsultas implements ILogicaConsultas
{
    /**
     * Lógica para el manejo de transacciones con la base de datos.
     */
    @Autowired
    private ILogicaAbstract logicaAbstract;
    
    @Override
    @Transactional(readOnly = true)
    public List<Object[]> consultarEntidades(String namedQuery, Map<String, Object> parametros)
            throws Exception
    {
        return logicaAbstract.consultarEntidades(namedQuery, parametros);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Object> consultarEntidades(String namedQuery, Map<String, Object> parametros, String clase)
            throws Exception
    {
        return logicaAbstract.consultarEntidades(namedQuery, parametros, clase);
    }

    @Override
    @Transactional(readOnly = true)
    public Object consultarRegistro(String namedQuery, Map<String, Object> parametros) throws Exception
    {
        return logicaAbstract.consultarRegistro(namedQuery, parametros);
    }
}
