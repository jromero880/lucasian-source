/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.entidades;

/**
 * Clase para definir y referenciar los nombres de los querys que ser�n utilizados en cada uno de los
 * componentes del proyecto.
 * 
 * @author Joan Romero
 */
public class QuerysComp
{
    public static final String PROC_PROVEEDOR = "Proveedor";
    public static final String PROC_CLIENTE = "Cliente";
    public static final String PROC_ARTICULO = "Articulo";
}
