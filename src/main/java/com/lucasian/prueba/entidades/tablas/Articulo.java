/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.entidades.tablas;

import java.io.Serializable;

/**
 * Entidad encarga de mapear los campos de la tabla de {@code articulo} de la base de datos.
 * 
 * @author Joan Romero
 */
public class Articulo implements Serializable
{
    /**
     * N�mero de versi�n para serializaci�n de la entidad.
     */
    private static final long serialVersionUID = 1L;
    
    private String codigo;
    private String proveedor;
    private String nombre;
    private String caracteristicas;
    
    public Articulo()
    {
        
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public String getProveedor()
    {
        return proveedor;
    }

    public void setProveedor(String proveedor)
    {
        this.proveedor = proveedor;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getCaracteristicas()
    {
        return caracteristicas;
    }

    public void setCaracteristicas(String caracteristicas)
    {
        this.caracteristicas = caracteristicas;
    }
}
