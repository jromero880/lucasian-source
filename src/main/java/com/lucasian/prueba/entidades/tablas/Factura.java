/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.entidades.tablas;

import java.io.Serializable;
import java.util.Date;

/**
 * Entidad encarga de mapear los campos de la tabla de {@code factura} de la base de datos.
 * 
 * @author Joan Romero
 */
public class Factura implements Serializable
{
    /**
     * N�mero de versi�n para serializaci�n de la entidad.
     */
    private static final long serialVersionUID = 1L;
    
    private int numero;
    private Date fecha;
    private String cliente;
    
    public Factura()
    {
        
    }

    public int getNumero()
    {
        return numero;
    }

    public void setNumero(int numero)
    {
        this.numero = numero;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public String getCliente()
    {
        return cliente;
    }

    public void setCliente(String cliente)
    {
        this.cliente = cliente;
    }
}
