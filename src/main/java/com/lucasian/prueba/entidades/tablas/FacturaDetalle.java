/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.entidades.tablas;

import java.io.Serializable;

/**
 * Entidad encarga de mapear los campos de la tabla de {@code detalle_factura} de la base de datos.
 * 
 * @author Joan Romero
 */
public class FacturaDetalle implements Serializable
{
    /**
     * N�mero de versi�n para serializaci�n de la entidad.
     */
    private static final long serialVersionUID = 1L;
    
    private int numero;
    private int detalle;
    private String articulo;
    private int cantidad;
    private int precio;
    
    public FacturaDetalle()
    {
        
    }

    public int getNumero()
    {
        return numero;
    }

    public void setNumero(int numero)
    {
        this.numero = numero;
    }

    public int getDetalle()
    {
        return detalle;
    }

    public void setDetalle(int detalle)
    {
        this.detalle = detalle;
    }

    public String getArticulo()
    {
        return articulo;
    }

    public void setArticulo(String articulo)
    {
        this.articulo = articulo;
    }

    public int getCantidad()
    {
        return cantidad;
    }

    public void setCantidad(int cantidad)
    {
        this.cantidad = cantidad;
    }

    public int getPrecio()
    {
        return precio;
    }

    public void setPrecio(int precio)
    {
        this.precio = precio;
    }
}
