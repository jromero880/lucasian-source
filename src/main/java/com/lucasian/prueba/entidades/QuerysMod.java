/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.entidades;

/**
 * Clase para definir y referencias los nombres de los querys que ser�n utilizados en cada uno de los m�dulos
 * del proyecto.
 * 
 * @author Joan Romero
 */
public class QuerysMod
{
    public static final String PROC_PROVEEDORES = "Proveedores";
    public static final String PROC_CLIENTES = "Clientes";
    public static final String PROC_ARTICULOS = "Articulos";
    public static final String PROC_NUM_FACTURA = "NumeroFactura";
    public static final String PROC_FACTURAS = "Facturas";
    public static final String PROC_FACTURAS_DET = "FacturasDetalle";
    
    public static final String CONS_VENTAS_ART_MES = "VentaArticulosMes";
    public static final String CONS_ART_SIN_VENTAS_MES = "ArticulosSinVentaMes";
    public static final String CONS_CLIENTES_VAL_GASTADO = "ClientesValorGastado";
    
    public static final String PRUE_ELIM_DET_FACTURA = "EliminarDetalleFactura";
    public static final String PRUE_ELIM_FACTURA = "EliminarFactura";
}
