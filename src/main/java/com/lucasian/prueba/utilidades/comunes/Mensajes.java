/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.utilidades.comunes;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;

import org.primefaces.PrimeFaces;

/**
 * Encargada de administrar los mensajes. Utiliza {@code FacesMessage} con la posibilidad de tener ciertos
 * mensajes predeterminados o configurables de acuerdo a la necesidad.
 * 
 * @author Joan Romero
 */
public class Mensajes
{
    /**
     * Muestra mensaje de error en caso de que se presente un error durante un proceso de consulta.
     * 
     * @param ex Excepcion generada por el proceso y la cual se va a visualizar.
     */
    public static void errorConsulta(Exception ex)
    {
        String error = Mensajes.getFullException(ex, ex.getMessage());
        
        Mensajes.mostrarError("Error", 
            "Se ha presentado un error al consultar los datos\n\n" + error);
    }
    
    /**
     * Muestra mensaje de error indicando que se present� un error al actualizar un registro.
     * 
     * @param modulo Nombre del m�dulo del que se esta intentando actualizar.
     * @param ex Excepcion generada por el proceso y la cual se va a visualizar.
     */
    public static void errorActualizar(String modulo, Exception ex)
    {
        Mensajes.errorBaseDatos(modulo, "actualizar", ex);
    }
    
    /**
     * Muestra mensaje de error indicando que se present� un error al eliminar un registro.
     * 
     * @param modulo Nombre del m�dulo del que se esta intentando eliminar.
     * @param ex Excepcion generada por el proceso y la cual se va a visualizar.
     */
    public static void errorEliminar(String modulo, Exception ex)
    {
        Mensajes.errorBaseDatos(modulo, "eliminar", ex);
    }
    
    /**
     * Muestra mensaje de error al intentar realizar un proceso en la base de datos.
     * 
     * @param modulo Nombre del m�dulo del que se intenta realizar el proceso.
     * @param tipo Tipo de proceso realizado en la base de datos.
     * @param ex Excepcion generada por el proceso y la cual se va a visualizar.
     */
    public static void errorBaseDatos(String modulo, String tipo, Exception ex)
    {
        String error = Mensajes.getFullException(ex, ex.getMessage());
        
        String patronMensaje = "Se ha presentado un error al %s %s\n\n%s";
        String mensaje = String.format(patronMensaje, tipo, modulo, error);
        
        Mensajes.mostrarError("Error", mensaje);
    }
    
    /**
     * Obtiene toda la excepci�n generada por un proceso con base a una excepci�n principal.
     * 
     * @param ex Excepci�n obtenida para extraer su mensaje y otras posibles excepciones internas.
     * @param error Mensaje de error acumulado.
     * @return Texto con mensaje de error completo de una excepci�n.
     */
    public static String getFullException(Throwable ex, String error)
    {
        Throwable innerEx = ex.getCause();
        if (innerEx != null)
        {
            String mensaje = innerEx.getMessage();
            if (mensaje != null)
            {
                error += "\n" + mensaje;
            }
            
            error = Mensajes.getFullException(innerEx, error);
        }
        
        return error;
    }
    
    /**
     * Visualiza un {@code FacesMessage} configurado para indicar que es un mensaje de error.
     * 
     * @param titulo Titulo para mensaje de error.
     * @param mensaje Descripci�n del error a ser visualizado.
     */
    public static void mostrarError(String titulo, String mensaje)
    {
        Mensajes.mostrarMensaje(FacesMessage.SEVERITY_ERROR, titulo, mensaje);
    }
    
    /**
     * Muestra mensaje de advertencia indicando que hay campos vac�os en un formulario. Es genérico, no
     * detalla cuales son los campos que faltan.
     */
    public static void camposVacios()
    {
        Mensajes.mostrarAdv("Advertencia", "Ha dejado campos vac�os. Por favor verifique los campos");
    }
    
    /**
     * Muestra mensaje de advertencia indicando que para un m�dulo ha dejado campos vac�os. No detalla cuales
     * son los campos que faltan por diligenciar.
     * 
     * @param modulo Nombre del m�dulo del cual se dejaron campos vac�os.
     */
    public static void camposVacios(String modulo)
    {
        String patronMensaje = "Ha dejado campos vac�os para %s. Por favor verifique los campos";
        String mensaje = String.format(patronMensaje, modulo);
        
        Mensajes.mostrarAdv("Advertencia", mensaje);
    }
    
    /**
     * Visualiza un {@code FacesMessage} configurado para indicar que es un mensaje de advertencia.
     * 
     * @param titulo Titulo para mensaje de advertencia.
     * @param mensaje Descripci�n de la advertencia a ser visualizada.
     */
    public static void mostrarAdv(String titulo, String mensaje)
    {
        Mensajes.mostrarMensaje(FacesMessage.SEVERITY_WARN, titulo, mensaje);
    }
    
    /**
     * Muestra mensaje de informaci�n indicando que un registro ha sido actualizado.
     * 
     * @param modulo Nombre del m�dulo donde se ha actualizado.
     */
    public static void infoActualizar(String modulo)
    {
        Mensajes.infoBaseDatos(modulo, "actualizado", "Guardado");
    }
    
    /**
     * Muestra mensaje de informaci�n indicando que un registro ha sido eliminado.
     * 
     * @param modulo Nombre del m�dulo donde se ha eliminado.
     */
    public static void infoEliminar(String modulo)
    {
        Mensajes.infoBaseDatos(modulo, "eliminado", "Eliminado");
    }
    
    /**
     * Muestra mensaje indicando que ha sido realizada una operaci�n en la base de datos.
     * 
     * @param modulo Nombre del m�dulo del que se realiza el proceso
     * @param tipo Tipo de proceso realizado en la base de datos.
     * @param titulo Titulo para el mensaje de informaci�n.
     */
    public static void infoBaseDatos(String modulo, String tipo, String titulo)
    {
        String patronMensaje = "Ha %s correctamente %s";
        String mensaje = String.format(patronMensaje, tipo, modulo);
        
        Mensajes.mostrarInfo(titulo, mensaje);
    }
    
    /**
     * Visualiza un {@code FacesMessage} configurado para indicar que es un mensaje de informaci�n.
     * 
     * @param titulo Titulo para mensaje de informaci�n.
     * @param mensaje Descripci�n de la informaci�n a ser visualizada.
     */
    public static void mostrarInfo(String titulo, String mensaje)
    {
        Mensajes.mostrarMensaje(FacesMessage.SEVERITY_INFO, titulo, mensaje);
    }
    
    /**
     * Visualiza un {@code FacesMessage} configurado para indicar a que tipo de mensaje corresponde.
     * 
     * @param severidad Severidad de mensaje a ser visualizado.
     * @param titulo Titulo para el mensaje.
     * @param mensaje Descripci�n del mensaje a ser visualizada.
     */
    public static void mostrarMensaje(Severity severidad, String titulo, String mensaje)
    {
        PrimeFaces.current().dialog().showMessageDynamic(new FacesMessage(severidad, titulo, mensaje));
    }
}
