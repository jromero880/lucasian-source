/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.utilidades.comunes;

import java.util.List;

import org.primefaces.component.calendar.Calendar;
import org.primefaces.component.inputmask.InputMask;
import org.primefaces.component.inputnumber.InputNumber;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.inputtextarea.InputTextarea;
import org.primefaces.component.selectonemenu.SelectOneMenu;

import com.lucasian.prueba.vistas.Campos;
import com.lucasian.prueba.vistas.Estados;

/**
 * Utilidades para manejo de componentes.
 * 
 * @author Joan Romero
 */
public class Componentes
{
    /**
     * Fija los datos de una consulta en los componentes correspondientes.
     * 
     * @param estado Estado del proceso. Solo se consulta si esta {@code CREANDO} o {@code MODIFICANDO}.
     * @param data Datos para fijarse en los componentes.
     * @param comp Listado de campos en el cual se fijar�n los datos.
     * @param tipoComp Listado con los tipos de campos que se usar�n.
     * @param tipo Tipo resultado de datos. {@code 0} para varios campos, {@code 1} para un solo campo.
     */
    public static void setDatosConsultaExternos(Estados estado, List<Object[]> data, Object[] comp, 
        Campos[] tipoComp, int tipo)
    {
        if (estado == Estados.MODIFICANDO || estado == Estados.CREANDO)
        {
            if (data != null && !data.isEmpty())
            {
                Object[] d = data.get(0);
                for (int i=0 ; i<d.length ; i++)
                {
                    if (tipoComp[i] == Campos.INPUTTEXT)
                    {
                        ((InputText)comp[i]).setValue(d[i]);
                    }
                    else if (tipoComp[i] == Campos.INPUTMASK)
                    {
                        ((InputMask)comp[i]).setValue(d[i]);
                    }
                }
            }
            else
            {
                if (tipo == 0)
                {
                    Componentes.limpiarExternosMenosCodigo(comp, tipoComp);
                }
                else
                {
                    if (tipoComp[0] == Campos.INPUTTEXT)
                    {
                        ((InputText)comp[0]).setValue("");
                    }
                    else if (tipoComp[0] == Campos.INPUTMASK)
                    {
                        ((InputMask)comp[0]).setValue("");
                    }
                }
            }
        }
    }
    
    /**
     * Limpia los componentes del listado excepto el primero, dando por entendido que ese corresponde a la
     * llave del registro.
     * 
     * @param comp Listado de campos en los cuales se fijar� el resultado de la consulta.
     * @param tipoComp Listado con los tipos de los campos que se usar�n.
     */
    private static void limpiarExternosMenosCodigo(Object[] comp, Campos[] tipoComp)
    {
        if (comp.length > 1)
        {
            for (int i=1 ; i<comp.length ; i++)
            {
                if (tipoComp[i] == Campos.INPUTTEXT)
                {
                    ((InputText)comp[i]).setValue("");
                }
                else if (tipoComp[i] == Campos.INPUTMASK)
                {
                    ((InputMask)comp[i]).setValue("");
                }
            }
        }
    }
    
    /**
     * Valida si en un conjunto de campos fue dejado alguno vacío.
     * 
     * @param campos Listado de campos que ser�n verificados.
     * @param tipos Listado con los tipos de los campos que se verificar�n.
     * @return Resultado de la validación. {@code true} si no hay campos vacíos indicando que son v�lidos.
     * {@code false} en caso de que algun campo tenga valores vacíos.
     */
    public static boolean validarCampos(Object[] campos, Campos[] tipos)
    {
        boolean validos = true;
        for (int i=0; i<campos.length; i++)
        {
            Object valor = "";
            
            if (tipos[i] == Campos.INPUTTEXT)
            {
                valor = ((InputText)campos[i]).getValue();
            }
            else if (tipos[i] == Campos.INPUTMASK)
            {
                valor = ((InputMask)campos[i]).getValue();
            }
            else if (tipos[i] == Campos.CALENDAR)
            {
                valor = ((Calendar)campos[i]).getValue();
            }
            else if (tipos[i] == Campos.INPUTNUMBER)
            {
                valor = ((InputNumber)campos[i]).getValue();
            }
            else if (tipos[i] == Campos.INPUTTEXTAREA)
            {
                valor = ((InputTextarea)campos[i]).getValue();
            }
            else if (tipos[i] == Campos.SELECTONEMENU)
            {
                valor = ((SelectOneMenu)campos[i]).getValue();
            }
            else
            {
                valor = "";
            }
            
            if (valor == null || valor.toString().equals(""))
            {
                validos = false;
                break;
            }
        }
        
        return validos;
    }
}
