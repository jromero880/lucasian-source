/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.utilidades.comunes;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 * Clase encargada de fijar y obtener datos en el contexto de la aplicación.
 * 
 * @author Joan Romero
 */
public class Faces
{
    /**
     * Obtiene el {@code ContextPath} de la aplicación.
     * 
     * @return {@code ContextPath}.
     */
    public static String getContextPath()
    {
        FacesContext context = FacesContext.getCurrentInstance();
        String path = context.getExternalContext().getRequestContextPath();
        return path;
    }
    
    /**
     * Obtiene la URL en el que se encuentra ejecutando la aplicación.
     * 
     * @return URL de la aplicación.
     */
    public static String getURL()
    {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext extcont = context.getExternalContext();
        
        String url = extcont.getRequestScheme() + "://" + extcont.getRequestServerName() + ":"
                + extcont.getRequestServerPort() + extcont.getRequestContextPath();
        
        return url;
    }
}
