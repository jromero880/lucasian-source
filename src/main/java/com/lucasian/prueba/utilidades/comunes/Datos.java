/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.utilidades.comunes;

/**
 * Utilidades para manejo de datos.
 * 
 * @author Joan Romero
 */
public class Datos
{
    /**
     * Recorre el conjunto de datos, identificando cual de ellos son {@code null} y los convierte en una
     * cadena vac�a.
     * 
     * @param data Conjunto de datos a analizar.
     * @return Conjunto de datos sin elementos {@code null}.
     */
    public static Object[] cambiarNull(Object[] data)
    {
        for (int i=0; i<data.length; i++)
        {
            if (data[i] == null)
            {
                data[i] = "";
            }
        }
        return data;
    }
}
