/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.lucasian.prueba.dto.procesos.ProveedorDTO;
import com.lucasian.prueba.entidades.QuerysMod;
import com.lucasian.prueba.entidades.tablas.Proveedor;
import com.lucasian.prueba.logica.consultas.ILogicaConsultas;
import com.lucasian.prueba.logica.procesos.ILogicaProveedor;

/**
 * Pruebas para las operaciones de proveedores con la base de datos (crear, modificar, eliminar y consultar).
 * 
 * @author Joan Romero
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/com/lucasian/prueba/spring/applicationContext.xml")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Proveedores
{
    @Autowired
    private ILogicaProveedor logicaProveedor;
    
    @Autowired
    private ILogicaConsultas logicaConsultas;
    
    /**
     * Prueba para validar que guarde un proveedor con CIF {@code 987654}.
     */
    @Test
    public void aGuardarProveedor()
    {
        try
        {
            Proveedor proveedor = new Proveedor();
            proveedor.setCif("987654");
            proveedor.setNombre("ALBERTO");
            proveedor.setApellido1("RAMIREZ");
            proveedor.setEmpresa("MI EMPRESA");
            
            logicaProveedor.guardarProveedor(proveedor);
            
            List<Object> data = getProveedor();
            
            assertFalse(data.isEmpty());
        }
        catch (Exception ex)
        {
            System.err.println(ex.getMessage());
            
            assertFalse(true);
        }
    }
    
    /**
     * Prueba para validar que se actualice la empresa del proveedor con CIF {@code 987654}.
     */
    @Test
    public void bEditarProveedor()
    {
        try
        {
            Proveedor proveedor = new Proveedor();
            
            proveedor.setCif("987654");
            proveedor.setNombre("ALBERTO");
            proveedor.setApellido1("RAMIREZ");
            proveedor.setEmpresa("MI EMPRESA SAS");
            
            logicaProveedor.editarProveedor(proveedor);
            
            List<Object> data = getProveedor();
            ProveedorDTO p = (ProveedorDTO)data.get(0);
            
            assertEquals("MI EMPRESA SAS", p.getEmpresa());
        }
        catch (Exception ex)
        {
            System.err.println(ex.getMessage());
            
            assertFalse(true);
        }
    }
    
    /**
     * Prueba para validar la eliminación del proveedor con CIF {@code 987654}.
     */
    @Test
    public void cEliminarProveedor()
    {
        try
        {
            Proveedor proveedor = new Proveedor();
            proveedor.setCif("987654");
            
            logicaProveedor.eliminarProveedor(proveedor);
            
            List<Object> data = getProveedor();
            
            assertTrue(data.isEmpty());
        }
        catch (Exception ex)
        {
            System.err.println(ex.getMessage());
            
            assertFalse(true);
        }
    }
    
    /**
     * Obtiene los datos del proveedor con CIF {@code 987654}.
     * 
     * @return Datos del proveedor.
     * @throws Exception Si se presenta algun error en el proceso.
     */
    private List<Object> getProveedor() throws Exception
    {
        Map<String, Object> param = new HashMap<>();
        param.put("proveedor", "987654");
        
        List<Object> data = logicaConsultas.consultarEntidades(QuerysMod.PROC_PROVEEDORES, param,
            "com.lucasian.prueba.dto.procesos.ProveedorDTO");
        
        return data;
    }
}
