/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.lucasian.prueba.dto.procesos.ClienteDTO;
import com.lucasian.prueba.entidades.QuerysMod;
import com.lucasian.prueba.entidades.tablas.Cliente;
import com.lucasian.prueba.logica.consultas.ILogicaConsultas;
import com.lucasian.prueba.logica.procesos.ILogicaCliente;

/**
 * Pruebas para las operaciones de clientes con la base de datos (crear, modificar, eliminar y consultar).
 * 
 * @author Joan Romero
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/com/lucasian/prueba/spring/applicationContext.xml")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Clientes
{
    @Autowired
    private ILogicaCliente logicaCliente;
    
    @Autowired
    private ILogicaConsultas logicaConsultas;
    
    /**
     * Prueba para validar que guarde un cliente con CIF {@code 123456}.
     */
    @Test
    public void aGuardarCliente()
    {
        try
        {
            Cliente cliente = new Cliente();
            cliente.setCif("123456");
            cliente.setNombre("JUAN");
            cliente.setApellido1("PEREZ");
            
            logicaCliente.guardarCliente(cliente);
            
            List<Object> data = getCliente();
            
            assertFalse(data.isEmpty());
        }
        catch (Exception ex)
        {
            System.err.println(ex.getMessage());
            
            assertFalse(true);
        }
    }
    
    /**
     * Prueba para validar que se actualice el nombre del cliente con CIF {@code 123456}.
     */
    @Test
    public void bEditarCliente()
    {
        try
        {
            Cliente cliente = new Cliente();
            cliente.setCif("123456");
            cliente.setNombre("JUAN ALBERTO");
            cliente.setApellido1("PEREZ");
            
            logicaCliente.editarCliente(cliente);
            
            List<Object> data = getCliente();
            ClienteDTO c = (ClienteDTO)data.get(0);
            
            assertEquals("JUAN ALBERTO", c.getNombre());
        }
        catch (Exception ex)
        {
            System.err.println(ex.getMessage());
            
            assertFalse(true);
        }
    }
    
    /**
     * Prueba para validar la eliminación del cliente con CIF {@code 123456}.
     */
    @Test
    public void cEliminarCliente()
    {
        try
        {
            Cliente cliente = new Cliente();
            cliente.setCif("123456");
            
            logicaCliente.eliminarCliente(cliente);
            
            List<Object> data = getCliente();
            
            assertTrue(data.isEmpty());
        }
        catch (Exception ex)
        {
            System.err.println(ex.getMessage());
            
            assertFalse(true);
        }
    }
    
    /**
     * Obtiene los datos del cliente con CIF {@code 123456}.
     * 
     * @return Datos del cliente.
     * @throws Exception Si se presenta algun error en el proceso.
     */
    private List<Object> getCliente() throws Exception
    {
        Map<String, Object> param = new HashMap<>();
        param.put("cliente", "123456");
        
        List<Object> data = logicaConsultas.consultarEntidades(QuerysMod.PROC_CLIENTES, param,
            "com.lucasian.prueba.dto.procesos.ClienteDTO");
        
        return data;
    }
}
