/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.lucasian.prueba.dto.procesos.ArticuloDTO;
import com.lucasian.prueba.entidades.QuerysMod;
import com.lucasian.prueba.entidades.tablas.Articulo;
import com.lucasian.prueba.entidades.tablas.Proveedor;
import com.lucasian.prueba.logica.consultas.ILogicaConsultas;
import com.lucasian.prueba.logica.procesos.ILogicaArticulo;
import com.lucasian.prueba.logica.procesos.ILogicaProveedor;

/**
 * Pruebas para las operaciones de art�culos con la base de datos (crear, modificar, eliminar y consultar).
 * 
 * @author Joan Romero
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/com/lucasian/prueba/spring/applicationContext.xml")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Component
public class Articulos
{
    @Autowired
    private ILogicaProveedor logicaProveedor;
    
    @Autowired
    private ILogicaArticulo logicaArticulo;
    
    @Autowired
    private ILogicaConsultas logicaConsultas;
    
    private static ILogicaProveedor logProveedor;
    
    /**
     * Establece el valor del l�gicas por inyecci�n a las est�ticas.
     */
    @PostConstruct
    public void initDelegado()
    {
        logProveedor = this.logicaProveedor;
    }
    
    /**
     * Inserta el proveedor con CIF {@code 456789} para asignar al art�culo.
     */
    @BeforeClass
    public static void iniciar()
    {
        try
        {
            Proveedor proveedor = new Proveedor();
            proveedor.setCif("456789");
            String res = logProveedor.guardarProveedor(proveedor);
            
            assertEquals("ejecutado", res);
        }
        catch (Exception ex) 
        {
            System.err.println(ex.getMessage());
            
            assertFalse(true);
        }
    }
    
    /**
     * Prueba para validar que guarde un art�culo con c�digo {@code 00099}.
     */
    @Test
    public void aGuardarArticulo()
    {
        try
        {
            Articulo articulo = new Articulo();
            articulo.setCodigo("00099");
            articulo.setNombre("BULTO DE NARANJA");
            articulo.setProveedor("456789");
            
            logicaArticulo.guardarArticulo(articulo);
            
            List<Object> data = getArticulo();
            
            assertFalse(data.isEmpty());
        }
        catch (Exception ex)
        {
            System.err.println(ex.getMessage());
            
            assertFalse(true);
        }
    }
    
    /**
     * Prueba para validar que se actualice el nombre del art�culo con c�digo {@code 00099}.
     */
    @Test
    public void bEditarArticulo()
    {
        try
        {
            Articulo articulo = new Articulo();
            articulo.setCodigo("00099");
            articulo.setNombre("BULTO DE NARANJA ORO MIEL");
            articulo.setProveedor("456789");
            
            logicaArticulo.editarArticulo(articulo);
            
            List<Object> data = getArticulo();
            ArticuloDTO a = (ArticuloDTO)data.get(0);
            
            assertEquals("BULTO DE NARANJA ORO MIEL", a.getNombre());
        }
        catch (Exception ex)
        {
            System.err.println(ex.getMessage());
            
            assertFalse(true);
        }
    }
    
    /**
     * Prueba para validar la eliminaci�n del art�culo con c�digo {@code 00099}.
     */
    @Test
    public void cEliminarArticulo()
    {
        try
        {
            Articulo articulo = new Articulo();
            articulo.setCodigo("00099");
            
            logicaArticulo.eliminarArticulo(articulo);
            
            List<Object> data = getArticulo();
            
            assertTrue(data.isEmpty());
        }
        catch (Exception ex)
        {
            System.err.println(ex.getMessage());
            
            assertFalse(true);
        }
    }
    
    /**
     * Obtiene los datos del art�culo con c�digo {@code 00099}.
     * 
     * @return Datos del proveedor.
     * @throws Exception Si se presenta algun error en el proceso.
     */
    private List<Object> getArticulo() throws Exception
    {
        Map<String, Object> param = new HashMap<>();
        param.put("articulo", "00099");
        
        List<Object> data = logicaConsultas.consultarEntidades(QuerysMod.PROC_ARTICULOS, param,
            "com.lucasian.prueba.dto.procesos.ArticuloDTO");
        
        return data;
    }
    
    /**
     * Eliminar el proveedor con CIF {@code 456789}.
     */
    @AfterClass
    public static void finalizar()
    {
        try
        {
            Proveedor proveedor = new Proveedor();
            proveedor.setCif("456789");
            
            String res = logProveedor.eliminarProveedor(proveedor);
            
            assertEquals("ejecutado", res);
        }
        catch (Exception ex)
        {
            System.err.println(ex.getMessage());
            
            assertFalse(true);
        }
    }
}
