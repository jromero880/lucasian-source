/* 
 * Copyright (c) 2020, Joan Romero.
 * Todos los Derechos Reservados.
 * 
 * Este es un software propietario y su contenido es confidencial de Joan Romero.
 */

package com.lucasian.prueba.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.lucasian.prueba.dto.procesos.FacturaDetalleDTO;
import com.lucasian.prueba.entidades.tablas.Articulo;
import com.lucasian.prueba.entidades.tablas.Cliente;
import com.lucasian.prueba.entidades.tablas.Factura;
import com.lucasian.prueba.entidades.tablas.Proveedor;
import com.lucasian.prueba.logica.procesos.ILogicaArticulo;
import com.lucasian.prueba.logica.procesos.ILogicaCliente;
import com.lucasian.prueba.logica.procesos.ILogicaFactura;
import com.lucasian.prueba.logica.procesos.ILogicaProveedor;

/**
 * Pruebas para las operaciones de facturas.
 * 
 * @author Joan Romero
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/com/lucasian/prueba/spring/applicationContext.xml")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Component
public class Facturas
{
    @Autowired
    private ILogicaCliente logicaCliente;
    
    @Autowired
    private ILogicaProveedor logicaProveedor;
    
    @Autowired
    private ILogicaArticulo logicaArticulo;
    
    @Autowired
    private ILogicaFactura logicaFactura;
    
    private static ILogicaCliente logCliente;
    private static ILogicaProveedor logProveedor;
    private static ILogicaArticulo logArticulo;
    
    /**
     * Establece el valor del l�gicas por inyecci�n a las est�ticas.
     */
    @PostConstruct
    public void initDelegado()
    {
        logCliente = this.logicaCliente;
        logProveedor = this.logicaProveedor;
        logArticulo = this.logicaArticulo;
    }
    
    /**
     * Inserta los datos por defecto para la factura de la siguiente manera:
     * <li> Cliente con CIF {@code 246802}
     * <li> Proveedor con CIF {@code 135791}
     * <li> Art�culo con c�digo {@code 00111}
     */
    @BeforeClass
    public static void iniciar()
    {
        try
        {
            Cliente cliente = new Cliente();
            cliente.setCif("246802");
            String resCli = logCliente.guardarCliente(cliente);
            
            assertEquals("ejecutado", resCli);
            
            Proveedor proveedor = new Proveedor();
            proveedor.setCif("135791");
            String resProv = logProveedor.guardarProveedor(proveedor);
            
            assertEquals("ejecutado", resProv);
            
            Articulo articulo = new Articulo();
            articulo.setCodigo("00111");
            articulo.setProveedor("135791");
            String resArt = logArticulo.guardarArticulo(articulo);
            
            assertEquals("ejecutado", resArt);
        }
        catch (Exception ex) 
        {
            System.err.println(ex.getMessage());
            
            assertFalse(true);
        }
    }
    
    /**
     * Prueba para validar que guarde la factura n�mero {@code 100}.
     */
    @Test
    public void aGuardarFactura()
    {
        try
        {
            Factura factura = new Factura();
            factura.setNumero(100);
            factura.setFecha(new Date());
            factura.setCliente("246802");
            
            List<FacturaDetalleDTO> articulos = new ArrayList<>();
            
            FacturaDetalleDTO art = new FacturaDetalleDTO();
            art.setCodArticulo("00111");
            art.setDetalle(1);
            art.setCantidad(10);
            art.setPrecio(5000);
            
            articulos.add(art);
            
            String res = logicaFactura.guardarFactura(factura, articulos);
            
            assertEquals("ejecutado", res);
        }
        catch (Exception ex)
        {
            System.err.println(ex.getMessage());
            
            assertFalse(true);
        }
    }
    
    /**
     * Valida el siguiente n�mero de factura, que debe ser posterior a la n�mero {@code 100} registrada.
     */
    @Test
    public void bNumeroFactura()
    {
        try
        {
            int num = logicaFactura.getNumeroFactura();
            
            assertEquals(101, num);
        }
        catch (Exception ex)
        {
            System.err.println(ex.getMessage());
            
            assertFalse(true);
        }
    }
    
    /**
     * Elimina la factura y el detalle de la factura n�mero {@code 100}.
     */
    @Test
    public void cEliminarArticulo()
    {
        try
        {
            String res = logicaFactura.eliminarFactura(100);
            
            assertEquals("ejecutado", res);
        }
        catch (Exception ex)
        {
            System.err.println(ex.getMessage());
            
            assertFalse(true);
        }
    }
    
    /**
     * Eliminar los datos por defecto creados para la factura.
     */
    @AfterClass
    public static void finalizar()
    {
        try
        {
            Articulo articulo = new Articulo();
            articulo.setCodigo("00111");
            String resArt = logArticulo.eliminarArticulo(articulo);
            
            assertEquals("ejecutado", resArt);
            
            Cliente cliente = new Cliente();
            cliente.setCif("246802");
            String resCli = logCliente.eliminarCliente(cliente);
            
            assertEquals("ejecutado", resCli);
            
            Proveedor proveedor = new Proveedor();
            proveedor.setCif("135791");
            String resProv = logProveedor.eliminarProveedor(proveedor);
            
            assertEquals("ejecutado", resProv);
        }
        catch (Exception ex)
        {
            System.err.println(ex.getMessage());
            
            assertFalse(true);
        }
    }
}
